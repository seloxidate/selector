use dracaena::ast::{
    column::*, constant::*, eval::*, eval_name::*, expression::*, factor::*, field::*,
    field_column::*, find::*, from::*, function::*, index_hint::*, intrinsics::*, join::*,
    operator::*, order_elem::*, order_group::*, parm::*, qualifier::*, select_option::*, table::*,
    term::*,
};
use dracaena::visitor::visitable::*;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;
use std::iter::FromIterator;

#[derive(Debug, PartialEq, Eq)]
pub enum SemanticError {
    OrdCompare,
    NoRefOfTableInWhereSelect,
    NoRefOfTableInWhereJoin(NoRefTo),
}

#[derive(Debug, PartialEq, Eq)]
pub enum NoRefTo {
    RootTable,
    JoinedTable,
}

impl fmt::Display for SemanticError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "This is a semantic error: {:?}", self)
    }
}

impl std::error::Error for SemanticError {
    // we use default impl here
}

#[derive(Debug, PartialEq, Eq)]
pub struct CheckSemanticVisitor<'a> {
    err_list: Vec<SemanticError>,
    table_names_in_select_where: Option<Vec<&'a str>>,
    table_names_in_join_where: Option<HashMap<&'a str, HashSet<&'a str>>>,
    table_names_in_join: Option<Vec<&'a str>>,
}

impl<'a> CheckSemanticVisitor<'a> {
    fn new() -> Self {
        Self {
            err_list: Default::default(),
            table_names_in_select_where: Default::default(),
            table_names_in_join_where: Default::default(),
            table_names_in_join: Default::default(),
        }
    }

    pub fn has_errors(&self) -> bool {
        !self.err_list.is_empty()
    }
}

fn get_table_names_from_factor(factor: &Factor) -> Vec<&str> {
    let mut table_names = vec![];
    if let Some((table_name, _)) = factor.table_and_field() {
        table_names.push(table_name);
    } else if let Factor::ParenthesizedTernary(ref tern) = factor {
        table_names.append(&mut get_table_names_in_where_from_tern(tern));
    }
    table_names
}

fn get_table_names_in_where_from_cond(cond: &ConditionalExpression) -> Vec<&str> {
    let mut table_names = vec![];
    match cond {
        ConditionalExpression::Expression(expr) if expr.factor().is_some() => {
            let mut table_names_from_fact = expr.factor().map(get_table_names_from_factor);
            if let Some(tnff) = table_names_from_fact.as_mut() {
                table_names.append(tnff);
            }
        }
        ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left,
            cond_expr_right,
            ..
        }) => {
            table_names.append(&mut get_table_names_in_where_from_cond(cond_expr_left));
            table_names.append(&mut get_table_names_in_where_from_cond(cond_expr_right));
        }
        ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(comp_fact_left)),
            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(comp_fact_right)),
            ..
        })) => {
            match (
                comp_fact_left
                    .factor()
                    .map(get_table_names_from_factor)
                    .as_mut(),
                comp_fact_right
                    .factor()
                    .map(get_table_names_from_factor)
                    .as_mut(),
            ) {
                (Some(tnff_left), Some(tnff_right)) => {
                    table_names.append(tnff_left);
                    table_names.append(tnff_right);
                }
                (Some(tnff_left), None) => {
                    table_names.append(tnff_left);
                }
                (None, Some(tnff_right)) => {
                    table_names.append(tnff_right);
                }
                _ => {} // nothing
            }
        }
        _ => {}
    }
    table_names
}

fn get_table_names_in_where_from_tern(tern: &TernaryExpression) -> Vec<&str> {
    let mut table_names = vec![];
    match tern {
        TernaryExpression::ConditionalExpression(cond) => {
            table_names.append(&mut get_table_names_in_where_from_cond(cond));
        }
        TernaryExpression::TernaryExpression(if_expr) => {
            table_names.append(&mut get_table_names_in_where_from_tern(&if_expr.if_branch));
            table_names.append(&mut get_table_names_in_where_from_tern(
                &if_expr.else_branch,
            ));
        }
    }
    table_names
}

impl<'a> VisitorEnterLeave<'a> for CheckSemanticVisitor<'a> {
    fn leave_visit_find_join(&mut self, node: &'a FindJoin) -> bool {
        if let Some(t_names_in_join_where) = &self.table_names_in_join_where {
            if !t_names_in_join_where.values().any(|hash_set_where| {
                hash_set_where.contains(
                    &node
                        .find_where
                        .find_order
                        .find_using
                        .select_table
                        .table_name(),
                )
            }) {
                self.err_list
                    .push(SemanticError::NoRefOfTableInWhereJoin(NoRefTo::RootTable));
            }

            let joins_that_have_no_ref_in_their_where: HashMap<_, _> = t_names_in_join_where
                .iter()
                .filter(|(join_name, hash_set_with_table_names)| {
                    !hash_set_with_table_names.contains(*join_name)
                })
                .collect();

            (0..joins_that_have_no_ref_in_their_where.len()).for_each(|_| {
                self.err_list
                    .push(SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable));
            })
        }
        true
    }
}

impl<'a> Visitor<'a> for CheckSemanticVisitor<'a> {
    fn visit_find_join(&mut self, _node: &'a FindJoin) {}

    fn visit_find_where(&mut self, node: &'a FindWhere) {
        self.table_names_in_select_where = node
            .if_expr
            .as_ref()
            .map(|tern| get_table_names_in_where_from_tern(tern));
    }

    fn visit_find_order(&mut self, _node: &'a FindOrder) {}

    fn visit_find_using(&mut self, _node: &'a FindUsing) {}

    fn visit_find_table(&mut self, _node: &'a FindTable) {}

    fn visit_select_table(&mut self, _node: &'a SelectTable) {}

    fn visit_select_stmt(&mut self, node: &'a SelectStmt) {
        if let Some(t_names_in_where) = &self.table_names_in_select_where {
            if !t_names_in_where.contains(&node.table.table_name()) {
                self.err_list.push(SemanticError::NoRefOfTableInWhereSelect);
            }
        }
    }

    fn visit_select_opts(&mut self, _node: &'a SelectOpts) {}

    fn visit_table(&mut self, _node: &'a Table) {}

    fn visit_from(&mut self, _node: &'a From) {}

    fn visit_index_hint(&mut self, _node: &'a IndexHint) {}

    fn visit_table_with_fields(&mut self, _node: &'a TableWithFields) {}

    fn visit_join_spec(&mut self, node: &'a JoinSpec) {
        let t_names_in_join_where = self.table_names_in_join_where.get_or_insert(HashMap::new());
        if let Some(tern) = node.if_expr.as_ref() {
            t_names_in_join_where.insert(
                node.join_order.join_using.join_clause.table.table_name(),
                HashSet::from_iter(get_table_names_in_where_from_tern(tern)),
            );
        }
    }

    fn visit_join_variant(&mut self, _node: &'a JoinVariant) {}

    fn visit_join_clause(&mut self, node: &'a JoinClause) {
        let t_names_in_join = self.table_names_in_join.get_or_insert(Vec::new());
        t_names_in_join.push(node.table.table_name());
    }

    fn visit_join_using(&mut self, _node: &'a JoinUsing) {}

    fn visit_join_order(&mut self, _node: &'a JoinOrder) {}

    fn visit_order_group_data(&mut self, _node: &'a OrderGroupData) {}

    fn visit_order_group(&mut self, _node: &'a OrderGroup) {}

    fn visit_group_order_by(&mut self, _node: &'a GroupOrderBy) {}

    fn visit_order_elem(&mut self, _node: &'a OrderElem) {}

    fn visit_direction(&mut self, _node: &'a Direction) {}

    fn visit_first_only(&mut self, _node: &'a FirstOnly) {}

    fn visit_force_placeholder_literal(&mut self, _node: &'a ForcePlaceholderLiteral) {}

    fn visit_function_expr(&mut self, _node: &'a FunctionExpression) {}

    fn visit_column(&mut self, _node: &'a Column) {}

    fn visit_conditional_expr(&mut self, _node: &'a ConditionalExpression) {}

    fn visit_conditional_expr_data(&mut self, _node: &'a ConditionalExpressionData) {}

    fn visit_expr(&mut self, _node: &'a Expression) {}

    fn visit_comparison_expr(&mut self, node: &'a ComparisonExpression) {
        if &node.comp_op != &ComparisonOperator::Equal
            && &node.comp_op != &ComparisonOperator::NotEqual
        {
            let mut left_right = (None, None);
            match &node.simple_expr_left {
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Constant(Constant::String(string_literal_left)),
                    },
                ))) => {
                    left_right.0 = Some(string_literal_left);
                }
                _ => {} // nothing to do
            };
            match &node.simple_expr_right {
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Constant(Constant::String(string_literal_right)),
                    },
                ))) => {
                    left_right.1 = Some(string_literal_right);
                }
                _ => {} // nothing to do
            };
            match left_right {
                (Some(_left), Some(_right)) => self.err_list.push(SemanticError::OrdCompare),
                (Some(_left), _) => self.err_list.push(SemanticError::OrdCompare),
                (_, Some(_right)) => self.err_list.push(SemanticError::OrdCompare),
                (_, _) => {} // nothing to do, because we have no literal
            };
        }
    }

    fn visit_as_expr(&mut self, _node: &'a AsExpression) {}

    fn visit_is_expr(&mut self, _node: &'a IsExpression) {}

    fn visit_simple_expr(&mut self, _node: &'a SimpleExpression) {}

    fn visit_simple_expr_data(&mut self, _node: &'a SimpleExpressionData) {}

    fn visit_term(&mut self, _node: &'a Term) {}

    fn visit_term_data(&mut self, _node: &'a TermData) {}

    fn visit_complement_factor(&mut self, _node: &'a ComplementFactor) {}

    fn visit_second_factor(&mut self, _node: &'a SecondFactor) {}

    fn visit_factor(&mut self, _node: &'a Factor) {}

    fn visit_constant(&mut self, _node: &'a Constant) {}

    fn visit_field(&mut self, _node: &'a Field) {}

    fn visit_function(&mut self, _node: &'a Function) {}

    fn visit_parm_elem(&mut self, _node: &'a ParmElem) {}

    fn visit_intrinsics(&mut self, _node: &'a Intrinsics) {}

    fn visit_eval(&mut self, _node: &'a Eval) {}

    fn visit_eval_name(&mut self, _node: &'a EvalName) {}

    fn visit_qualifier(&mut self, _node: &'a Qualifier) {}

    fn visit_if_expr(&mut self, _node: &'a IfExpression) {}

    fn visit_ternary_expr(&mut self, _node: &'a TernaryExpression) {}

    fn visit_logical_op(&mut self, _node: &'a LogicalOperator) {}

    fn visit_comparison_op(&mut self, _node: &'a ComparisonOperator) {}

    fn visit_field_list(&mut self, _node: &'a FieldList) {}
    fn visit_dcl_stmt(&mut self, _: &'a dracaena::ast::declaration::DeclarationStmt) {}
    fn visit_dcl_init_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationInitList) {}
    fn visit_dcl_comma_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationCommaList) {}
    fn visit_dcl_init(&mut self, _: &'a dracaena::ast::declaration::DeclarationInit) {}
    fn visit_dcl(&mut self, _: &'a dracaena::ast::declaration::Declaration) {}
    fn visit_assign_clause(&mut self, _: &'a dracaena::ast::assignment::AssignmentClause) {}
    fn visit_assign_stmt(&mut self, _: &'a dracaena::ast::assignment::AssignmentStmt) {}
    fn visit_id_with_assign_clause(
        &mut self,
        _: &'a dracaena::ast::declaration::IdWithAssigmentClause,
    ) {
    }
    fn visit_assign(&mut self, _: &'a dracaena::ast::assignment::Assign) {}
    fn visit_pre_post_inc_dec_assign(
        &mut self,
        _: &'a dracaena::ast::assignment::PrePostIncDecAssign,
    ) {
    }
    fn visit_assign_inc_dec(&mut self, _: &'a dracaena::ast::assignment::AssignIncDec) {}
    fn visit_stmt(&mut self, _: &'a dracaena::ast::statement::Statement) {}
    fn visit_expr_stmt(&mut self, _: &'a dracaena::ast::statement::ExpressionStatement) {}
    fn visit_local_body(&mut self, _: &'a dracaena::ast::local_body::LocalBody) {}
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::parsers::parser::SelectStmtParsed;

    #[test]
    fn check_semantic_has_errors() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    where custTable.AccountNum <= '4711';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert!(
            check_visitor.has_errors(),
            "Visitor should have stored errors, but has none"
        );
    }

    #[test]
    fn check_semantic_has_ord_compare_error() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    where custTable.AccountNum <= '4711';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert_eq!(check_visitor.err_list, vec![SemanticError::OrdCompare]);
    }

    #[test]
    fn check_semantic_has_multiple_ord_compare_errors() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    where custTable.AccountNum <= '4711'
                    exists join dirParty
                        where dirParty.RecId == custTable.Party
                            && 'foo' > 'bar';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![SemanticError::OrdCompare, SemanticError::OrdCompare]
        );
    }

    #[test]
    fn check_semantic_has_no_ref_of_table_in_where_select_error() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    where dirParty.Party == '4711';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![SemanticError::NoRefOfTableInWhereSelect]
        );
    }

    #[test]
    fn check_semantic_has_no_ref_of_table_in_where_select_error_nested() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    where (dirParty.Party == '4711'
                        && (dirParty.Foo == 'Foo' ? dirParty.Location == dirParty.Trans
                            : dirParty.Bar == foo.Bar));"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![SemanticError::NoRefOfTableInWhereSelect]
        );
    }

    #[test]
    fn check_semantic_table_referenced_in_test_expression_but_still_has_no_ref_of_table_in_where_select_error(
    ) {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    where custTable.Foo == 'Foo'
                        ? dirParty.Location == '4711'
                        : dirParty.Party == 'Foo';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![SemanticError::NoRefOfTableInWhereSelect]
        );
    }

    #[test]
    fn check_semantic_complicated_select_stmt_but_semantically_correct() {
        let mut check_visitor = CheckSemanticVisitor::new();

        // TODO: make this more complicated!!
        let parse_result = r#"select custTable
                    where (dirParty.Party == '4711'
                        && (dirParty.Foo == 'Foo' ? dirParty.Location == custTable.Location
                            : dirParty.Bar == custTable.Bar));"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor(&mut check_visitor);

        assert!(
            !check_visitor.has_errors(),
            "Select statement should be semantically correct."
        );
    }

    /// Although this is valid x++, it doesn't make sense to do a join without connecting
    /// it to the root table, so we treat this as an error.
    #[test]
    fn check_semantic_no_ref_to_root_table_has_no_ref_of_table_in_where_join_error() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    join dirParty
                        where dirParty.Party == '4711';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![SemanticError::NoRefOfTableInWhereJoin(NoRefTo::RootTable)]
        );
    }

    #[test]
    fn check_semantic_no_ref_to_root_table_multiple_joins_has_no_ref_of_table_in_where_join_error()
    {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    join dirParty
                        where dirParty.Party == '4711'
                    join logistics
                        where logistics.Address == 'Foo';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut check_visitor);

        let err_list_filtered: Vec<_> = check_visitor
            .err_list
            .iter()
            .filter(|err| match **err {
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::RootTable) => true,
                _ => false,
            })
            .collect();

        // we only expect one, although we have multiple joins with no ref to root
        assert_eq!(
            err_list_filtered,
            vec![&SemanticError::NoRefOfTableInWhereJoin(NoRefTo::RootTable)]
        );
    }

    #[test]
    fn check_semantic_no_ref_to_joined_table_has_no_ref_of_table_in_where_join_error() {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    join dirParty
                        where custTable.Party == '4711';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable)]
        );
    }

    #[test]
    fn check_semantic_no_ref_to_joined_table_multiple_joins_has_no_ref_of_table_in_where_join_error(
    ) {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    join dirParty
                        where custTable.Party == '4711'
                    join logistics
                        where custTable.Foo == 'bar';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable),
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable)
            ]
        );
    }

    #[test]
    fn check_semantic_no_ref_to_joined_table_local_multiple_joins_has_no_ref_of_table_in_where_join_error(
    ) {
        let mut check_visitor = CheckSemanticVisitor::new();

        // we have a ref to dirParty, but it is in another join!
        let parse_result = r#"select custTable
                    join dirParty
                        where custTable.Party == '4711'
                    join logistics
                        where dirParty.Foo == custTable.Bar;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable),
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable)
            ]
        );
    }

    #[test]
    fn check_semantic_no_ref_to_root_nor_to_joined_table_has_multiple_no_ref_of_table_in_where_join_error(
    ) {
        let mut check_visitor = CheckSemanticVisitor::new();

        let parse_result = r#"select custTable
                    join dirParty
                        where foo.Bar == '4711';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut check_visitor);

        assert_eq!(
            check_visitor.err_list,
            vec![
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::RootTable),
                SemanticError::NoRefOfTableInWhereJoin(NoRefTo::JoinedTable)
            ]
        );
    }
}
