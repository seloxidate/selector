use chumsky::{
    error::Rich,
    extra,
    input::ValueInput,
    primitive::{choice, just},
    Parser,
};
use dracaena::ast::{column::*, common::FieldIdentifier};

use super::common::{column_idx_chumsky, keyword_no_case, xpp_identifier_chumsky};

pub fn function_arg_chumsky<'a>(
) -> impl Parser<'a, &'a str, FieldIdentifier, extra::Err<Rich<'a, char>>> + Clone {
    xpp_identifier_chumsky().map(String::from)
}

fn column_function_chumsky_helper<
    'a,
    I: Clone + AsRef<str> + ValueInput<'a>,
    F: Fn(String) -> FunctionExpression + Clone,
>(
    keyword: I,
    fn_expr_out: F,
) -> impl Parser<'a, &'a str, FunctionExpression, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case(keyword)
        .ignore_then(
            function_arg_chumsky()
                .padded()
                .delimited_by(just('(').padded(), just(')')),
        )
        .map(fn_expr_out)
}

pub fn column_function_chumsky<'a>(
) -> impl Parser<'a, &'a str, FunctionExpression, extra::Err<Rich<'a, char>>> + Clone {
    choice((
        column_function_chumsky_helper("count", FunctionExpression::Count),
        column_function_chumsky_helper("avg", FunctionExpression::Avg),
        column_function_chumsky_helper("sum", FunctionExpression::Sum),
        column_function_chumsky_helper("minOf", FunctionExpression::MinOf),
        column_function_chumsky_helper("maxOf", FunctionExpression::MaxOf),
    ))
}

pub fn column_in_select_chumsky<'a>(
) -> impl Parser<'a, &'a str, Column, extra::Err<Rich<'a, char>>> + Clone {
    column_function_chumsky()
        .map(Column::from)
        .or(xpp_identifier_chumsky()
            .then(column_idx_chumsky().or_not())
            .map(|(ident, idx)| Column {
                name: String::from(ident),
                function: None,
                index: idx,
            }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    #[test]
    fn column_parse_func() {
        let column_name = "amount";

        assert_parse_chumsky(
            "avg(amount)",
            &Column {
                name: column_name.to_string(),
                function: Some(FunctionExpression::Avg(column_name.to_string())),
                index: None,
            },
            &column_in_select_chumsky,
        );
    }

    #[test]
    fn column_parse_func_with_whitepace_between_parens() {
        let column_name = "amount";

        assert_parse_chumsky(
            "avg ( amount )",
            &Column {
                name: column_name.to_string(),
                function: Some(FunctionExpression::Avg(column_name.to_string())),
                index: None,
            },
            &column_in_select_chumsky,
        );
    }

    #[test]
    fn column_parse_func_with_whitepace_at_end_should_err() {
        let should_err = "avg(amount) ";

        assert!(column_in_select_chumsky().parse(should_err).has_errors());
    }

    #[test]
    fn column_parse_func_case_insensitive() {
        let column_name = "amount";

        assert_parse_chumsky(
            "mAXoF(amount)",
            &Column {
                name: column_name.to_string(),
                function: Some(FunctionExpression::MaxOf(column_name.to_string())),
                index: None,
            },
            &column_in_select_chumsky,
        );
    }

    #[test]
    fn column_parse_idx() {
        let column_name_with_idx = "dimensions[1]";

        assert_parse_chumsky(
            column_name_with_idx,
            &Column {
                name: "dimensions".to_string(),
                function: None,
                index: Some(1),
            },
            &column_in_select_chumsky,
        );
    }

    #[test]
    fn column_parse_func_parse_no_idx() {
        let stmt_to_parse = "avg(amount)[42]";

        assert!(column_in_select_chumsky().parse(stmt_to_parse).has_errors());
    }

    #[test]
    fn column_parse_not_alphanumeric_err() {
        let s = ";foo";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_in_option_not_alphanumeric_err() {
        let s = ";mytable.foo";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_unknown_func_err() {
        let s = "foo(recId)";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_fully_qualified_with_table_unknown_func_err() {
        let s = "mytable.foo(recId)";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_multiple_err() {
        let s = "foo, bar";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_only_first_with_index_err() {
        let s = "foo[42], bar";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_fully_qualified_with_table_only_first_err() {
        let s = "mytable.foo, bar";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_fully_qualified_with_table_and_index_only_first_err() {
        let s = "mytable.foo[42], bar";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_terminated_by_whitespace() {
        let s = "foo ";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }

    #[test]
    fn column_parse_fully_qualified_terminated_by_whitespace() {
        let s = "mytable.foo ";
        assert!(column_in_select_chumsky().parse(s).has_errors());
    }
}
