use chumsky::Parser;
use dracaena::ast::find::FindJoin;

use std::fmt::Display;
use std::fmt::Formatter;
use std::str::FromStr;

use super::find::find_stmt_chumsky;

#[derive(Debug, PartialEq)]
pub struct SelectStmtParsed {
    pub ast: FindJoin,
    pub left_over: String,
}

#[derive(Debug, PartialEq)]
pub enum ParseError {
    All(String),
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "Custom parse error: {:?}", self)
    }
}

impl std::error::Error for ParseError {
    // we use default impls
}

impl FromStr for SelectStmtParsed {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let result = find_stmt_chumsky().parse(s).into_result().map_err(|errs| {
            ParseError::All(errs.into_iter().map(|e| e.into_owned().to_string()).fold(
                String::from("Error: "),
                |mut acc, e| {
                    acc.push_str(e.as_str());
                    acc.push('\n');
                    acc
                },
            ))
        });
        result.map(|ast| SelectStmtParsed {
            ast,
            left_over: String::new(),
        })
    }
}
