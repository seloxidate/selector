use chumsky::text::whitespace;
use chumsky::IterParser;
use chumsky::{error::Rich, extra, primitive::choice, Parser};
use dracaena::ast::select_option::*;
use std::str;

use super::common::keyword_no_case;

fn firstonly_chumsky<'a>() -> impl Parser<'a, &'a str, FirstOnly, extra::Err<Rich<'a, char>>> + Clone
{
    choice((
        keyword_no_case("firstonly1000").to(FirstOnly::FirstOnly1000),
        keyword_no_case("firstOnly100").to(FirstOnly::FirstOnly100),
        keyword_no_case("firstOnly10").to(FirstOnly::FirstOnly10),
        keyword_no_case("firstonly1").to(FirstOnly::FirstOnly1),
        keyword_no_case("firstOnly").to(FirstOnly::FirstOnly),
    ))
}

fn force_placeholder_literal_chumsky<'a>(
) -> impl Parser<'a, &'a str, ForcePlaceholderLiteral, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case("forcePlaceholders")
        .to(ForcePlaceholderLiteral::ForcePlaceholders)
        .or(keyword_no_case("forceLiterals").to(ForcePlaceholderLiteral::ForceLiterals))
}

pub fn select_option_chumsky<'a>(
) -> impl Parser<'a, &'a str, Vec<SelectOpts>, extra::Err<Rich<'a, char>>> + Clone {
    let select_opt = choice((
        keyword_no_case("firstFast").to(SelectOpts::FirstFast),
        firstonly_chumsky().map(SelectOpts::FirstOnly),
        keyword_no_case("forUpdate").to(SelectOpts::ForUpdate),
        keyword_no_case("forceNestedLoop").to(SelectOpts::ForceNestedLoop),
        force_placeholder_literal_chumsky().map(SelectOpts::ForcePlaceholderLiteral),
        keyword_no_case("forceSelectOrder").to(SelectOpts::ForceSelectOrder),
        keyword_no_case("noFetch").to(SelectOpts::NoFetch),
        keyword_no_case("repeatableRead").to(SelectOpts::RepeatableRead),
        keyword_no_case("reverse").to(SelectOpts::Reverse),
        keyword_no_case("validTimeState").to(SelectOpts::ValidTimeState),
    ))
    .separated_by(whitespace().at_least(1))
    .collect::<Vec<SelectOpts>>()
    .try_map(|select_opts: Vec<SelectOpts>, span| {
        let mut so_deduped = select_opts.clone();
        so_deduped.sort();
        so_deduped.dedup_by_key(|e| {
            let c = e.clone();
            Into::<DistinctSelectOpts<SelectOpts>>::into(c)
        });
        if so_deduped.len() == select_opts.len() {
            Ok(select_opts)
        } else {
            // TODO: provide a better error message which points out the duplicate entries;
            // we currently can't provide those elements as we are not storing them anywhere
            Err(Rich::custom(
                span,
                "Duplicate select options are not allowed.",
            ))
        }
    });

    select_opt
}

#[derive(PartialEq)]
enum DistinctSelectOpts<T> {
    FirstOnly,
    ForcePlaceholderLiteral,
    Other(T),
}

impl From<SelectOpts> for DistinctSelectOpts<SelectOpts> {
    fn from(value: SelectOpts) -> Self {
        match value {
            SelectOpts::FirstOnly(_) => DistinctSelectOpts::FirstOnly,
            SelectOpts::ForcePlaceholderLiteral(_) => DistinctSelectOpts::ForcePlaceholderLiteral,
            other => DistinctSelectOpts::Other(other),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    #[test]
    fn select_opt_single() {
        let select_opt = "forUpdate";

        let expected = vec![SelectOpts::ForUpdate];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }

    #[test]
    fn select_opt_simple() {
        let select_opt = "firstfast reverse";

        let expected = vec![SelectOpts::FirstFast, SelectOpts::Reverse];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }

    #[test]
    fn select_opt_simple_static_order_in_ast() {
        let select_opt = "reverse firstfast";

        let expected = vec![SelectOpts::Reverse, SelectOpts::FirstFast];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }

    #[test]
    fn select_opt_force_placeholders_alternative() {
        let select_opt = "forceplaceholders firstfast";

        let expected = vec![
            SelectOpts::ForcePlaceholderLiteral(ForcePlaceholderLiteral::ForcePlaceholders),
            SelectOpts::FirstFast,
        ];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }

    #[test]
    fn select_opt_firstonly_alternative() {
        let select_opt = "firstonly firstfast";

        let expected = vec![
            SelectOpts::FirstOnly(FirstOnly::FirstOnly),
            SelectOpts::FirstFast,
        ];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }

    #[test]
    fn select_opt_firstonly1000_alternative() {
        let select_opt = "firstonly1000 firstfast";

        let expected = vec![
            SelectOpts::FirstOnly(FirstOnly::FirstOnly1000),
            SelectOpts::FirstFast,
        ];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }

    #[test]
    fn select_opt_multiple_firstonly_should_err() {
        let select_opt = "firstonly firstonly100 firstfast";

        assert!(select_option_chumsky().parse(select_opt).has_errors());
    }

    #[test]
    fn select_opt_multiple_force_placeholder_literal_should_err() {
        let select_opt = "reverse firstfast forcePlaceholders forceLiterals";

        assert!(select_option_chumsky().parse(select_opt).has_errors());
    }

    #[test]
    fn select_opt_duplicate() {
        let select_opt = "forupdate firstfast forUpdate";

        assert!(select_option_chumsky().parse(select_opt).has_errors());
    }

    #[test]
    fn select_opt_all() {
        let select_opt = r#"reverse firstfast forcePlaceholders
            firstOnly forUpdate repeatableRead validTimeState nofetch
            forceSelectOrder forceNestedLoop"#;

        let expected = vec![
            SelectOpts::Reverse,
            SelectOpts::FirstFast,
            SelectOpts::ForcePlaceholderLiteral(ForcePlaceholderLiteral::ForcePlaceholders),
            SelectOpts::FirstOnly(FirstOnly::FirstOnly),
            SelectOpts::ForUpdate,
            SelectOpts::RepeatableRead,
            SelectOpts::ValidTimeState,
            SelectOpts::NoFetch,
            SelectOpts::ForceSelectOrder,
            SelectOpts::ForceNestedLoop,
        ];

        assert_parse_chumsky(select_opt, &expected, &select_option_chumsky);
    }
}
