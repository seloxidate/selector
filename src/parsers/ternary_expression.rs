use chumsky::error::Rich;
use chumsky::primitive::{just, recurse};

use chumsky::{extra, Parser};

use dracaena::ast::expression::{IfExpression, TernaryExpression};

use std::str;

use super::conditional_expression::conditional_expr_chumsky;

pub fn ternary_expr_chumsky<'a>(
) -> impl Parser<'a, &'a str, TernaryExpression, extra::Err<Rich<'a, char>>> + Clone {
    conditional_expr_chumsky()
        .then_ignore(just('?').padded())
        .then(
            recurse(ternary_expr_chumsky)
                .map(Box::new)
                .or(conditional_expr_chumsky().map(|cond_expr| {
                    Box::new(TernaryExpression::ConditionalExpression(cond_expr))
                })),
        )
        .then_ignore(just(':').padded())
        .then(
            recurse(ternary_expr_chumsky)
                .map(Box::new)
                .or(conditional_expr_chumsky().map(|cond_expr| {
                    Box::new(TernaryExpression::ConditionalExpression(cond_expr))
                })),
        )
        .map(|((test_expression, if_branch), else_branch)| {
            TernaryExpression::TernaryExpression(IfExpression {
                test_expression,
                if_branch,
                else_branch,
            })
        })
        .or(conditional_expr_chumsky().map(TernaryExpression::ConditionalExpression))
        .boxed()
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::expression::ComparisonExpression;
    use dracaena::ast::expression::ConditionalExpression;
    use dracaena::ast::expression::Expression;
    use dracaena::ast::expression::IfExpression;
    use dracaena::ast::expression::SimpleExpression;
    use dracaena::ast::factor::*;
    use dracaena::ast::field::Field;
    use dracaena::ast::operator::ComparisonOperator;
    use dracaena::ast::qualifier::Qualifier;
    use dracaena::ast::term::Term;

    #[test]
    fn ternary_expr_simple() {
        let expr_str = "foo ? bar : baz";

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    })),
                ))),
            )),
            else_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("baz".to_owned())),
                    })),
                ))),
            )),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }

    #[test]
    fn ternary_expr_deep_ifs() {
        let expr_str = r#"foo
            ? bar
                ? baz : quux
            : drix"#;

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::TernaryExpression(IfExpression {
                test_expression: ConditionalExpression::Expression(Expression::Simple(
                    SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                        SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("bar".to_owned())),
                        },
                    ))),
                )),
                if_branch: Box::new(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                        Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("baz".to_owned())),
                        })),
                    ))),
                )),
                else_branch: Box::new(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                        Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("quux".to_owned())),
                        })),
                    ))),
                )),
            })),
            else_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("drix".to_owned())),
                    })),
                ))),
            )),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }

    #[test]
    fn ternary_expr_deep_else() {
        let expr_str = r#"foo
            ? bar :
                baz ? quux
                    : drix"#;

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    })),
                ))),
            )),
            else_branch: Box::new(TernaryExpression::TernaryExpression(IfExpression {
                test_expression: ConditionalExpression::Expression(Expression::Simple(
                    SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                        SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("baz".to_owned())),
                        },
                    ))),
                )),
                if_branch: Box::new(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                        Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("quux".to_owned())),
                        })),
                    ))),
                )),
                else_branch: Box::new(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                        Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("drix".to_owned())),
                        })),
                    ))),
                )),
            })),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }

    #[test]
    fn ternary_expr_parenthesized() {
        let expr_str = r#"foo
            ? (bar ? baz : quux)
            : drix"#;

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::TernaryExpression(IfExpression {
                                test_expression: ConditionalExpression::Expression(
                                    Expression::Simple(SimpleExpression::Term(
                                        Term::ComplementFactor(ComplementFactor::SecondFactor(
                                            SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("bar".to_owned())),
                                            },
                                        )),
                                    )),
                                ),
                                if_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("baz".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                                else_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("quux".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                            }),
                        )),
                    })),
                ))),
            )),
            else_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("drix".to_owned())),
                    })),
                ))),
            )),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }

    #[test]
    fn ternary_expr_not_parenthesized_not_variable() {
        let expr_str = r#"foo
            ? !(bar ? !baz : quux)
            : drix"#;

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::NotSecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::TernaryExpression(IfExpression {
                                test_expression: ConditionalExpression::Expression(
                                    Expression::Simple(SimpleExpression::Term(
                                        Term::ComplementFactor(ComplementFactor::SecondFactor(
                                            SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("bar".to_owned())),
                                            },
                                        )),
                                    )),
                                ),
                                if_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::NotSecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("baz".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                                else_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("quux".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                            }),
                        )),
                    })),
                ))),
            )),
            else_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("drix".to_owned())),
                    })),
                ))),
            )),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }

    #[test]
    fn ternary_expr_not_parenthesized_comparison_op() {
        let expr_str = r#"foo
            ? bar == baz : quux.drix"#;

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                    simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("bar".to_owned())),
                        }),
                    )),
                    comp_op: ComparisonOperator::Equal,
                    simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("baz".to_owned())),
                        }),
                    )),
                })),
            )),
            else_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::QualifierId(
                            Qualifier::Id("quux".to_owned()),
                            "drix".to_owned(),
                        )),
                    })),
                ))),
            )),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }

    #[test]
    fn ternary_expr_test_expr_is_parenthesized_ternary_expr() {
        let expr_str = r#"(foo ? bar : baz)
            ? (quux ? drax : corge)
            : grault"#;

        let expected = TernaryExpression::TernaryExpression(IfExpression {
            test_expression: ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::TernaryExpression(IfExpression {
                                test_expression: ConditionalExpression::Expression(
                                    Expression::Simple(SimpleExpression::Term(
                                        Term::ComplementFactor(ComplementFactor::SecondFactor(
                                            SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("foo".to_owned())),
                                            },
                                        )),
                                    )),
                                ),
                                if_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("bar".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                                else_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("baz".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                            }),
                        )),
                    },
                ))),
            )),
            if_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::TernaryExpression(IfExpression {
                                test_expression: ConditionalExpression::Expression(
                                    Expression::Simple(SimpleExpression::Term(
                                        Term::ComplementFactor(ComplementFactor::SecondFactor(
                                            SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("quux".to_owned())),
                                            },
                                        )),
                                    )),
                                ),
                                if_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id("drax".to_owned())),
                                            }),
                                        )),
                                    )),
                                )),
                                else_branch: Box::new(TernaryExpression::ConditionalExpression(
                                    ConditionalExpression::Expression(Expression::Simple(
                                        SimpleExpression::Term(Term::ComplementFactor(
                                            ComplementFactor::SecondFactor(SecondFactor {
                                                sign_op: None,
                                                factor: Factor::Field(Field::Id(
                                                    "corge".to_owned(),
                                                )),
                                            }),
                                        )),
                                    )),
                                )),
                            }),
                        )),
                    })),
                ))),
            )),
            else_branch: Box::new(TernaryExpression::ConditionalExpression(
                ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("grault".to_owned())),
                    })),
                ))),
            )),
        });

        assert_parse_chumsky(expr_str, &expected, &ternary_expr_chumsky);
    }
}
