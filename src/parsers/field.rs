use chumsky::prelude::*;
use dracaena::ast::field::Field;

use super::common::xpp_identifier_chumsky;
use super::qualifier::qualifier_chumsky;

pub fn field_chumsky<'a>() -> impl Parser<'a, &'a str, Field, extra::Err<Rich<'a, char>>> + Clone {
    xpp_identifier_chumsky()
        .then_ignore(just('.').not())
        .map(|id| Field::Id(id.to_string()))
        .or(qualifier_chumsky()
            .then(xpp_identifier_chumsky())
            .map(|(quali, id)| Field::QualifierId(quali, id.to_string())))
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::qualifier::Qualifier;

    #[test]
    fn field_simple_field() {
        assert_parse_chumsky("fooBar", &Field::Id("fooBar".to_owned()), &field_chumsky);
    }

    #[test]
    fn field_with_qualifier_simple() {
        assert_parse_chumsky(
            "foo.bar",
            &Field::QualifierId(Qualifier::Id("foo".to_owned()), "bar".to_owned()),
            &field_chumsky,
        );
    }
}
