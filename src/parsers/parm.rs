use chumsky::{error::Rich, extra, primitive::just, text::whitespace, IterParser, Parser};
use dracaena::ast::parm::ParmElem;

use super::ternary_expression::ternary_expr_chumsky;

pub fn parm_elem_chumsky<'a>(
) -> impl Parser<'a, &'a str, ParmElem, extra::Err<Rich<'a, char>>> + Clone {
    ternary_expr_chumsky().map(|te| ParmElem::TernaryExpression(Box::new(te)))
}

pub fn parm_list_chumsky<'a>(
) -> impl Parser<'a, &'a str, Option<Vec<ParmElem>>, extra::Err<Rich<'a, char>>> + Clone {
    whitespace()
        .ignore_then(just(')'))
        .to(None)
        .or(parm_elem_chumsky()
            .separated_by(just(',').padded())
            .at_least(1)
            .collect::<Vec<ParmElem>>()
            .then_ignore(just(')'))
            .map(Some))
}

#[cfg(test)]
mod tests {

    use super::*;

    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::eval::Eval;
    use dracaena::ast::eval_name::EvalName;

    use dracaena::ast::factor::*;
    use dracaena::ast::field::Field;
    use dracaena::ast::qualifier::Qualifier;

    #[test]
    fn simple_parm_elem() {
        let parm_elem_expr = "foo.bar()";

        let ast_expected = ParmElem::TernaryExpression(Box::new(
            Factor::Eval(Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("foo".to_owned()), "bar".to_owned()),
                parm_list: None,
            })
            .builder()
            .into(),
        ));

        assert_parse_chumsky(parm_elem_expr, &ast_expected, &parm_elem_chumsky);
    }

    #[test]
    fn parm_list_no_elems() {
        let parm_elem_expr = ")";

        let output = parm_list_chumsky().parse(parm_elem_expr).into_result();

        assert_eq!(output, Ok(None));
    }

    #[test]
    fn parm_list_one_elem() {
        let parm_list_expr = "foo.bar())";

        let ast_expected = Some(vec![ParmElem::TernaryExpression(Box::new(
            Factor::Eval(Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("foo".to_owned()), "bar".to_owned()),
                parm_list: None,
            })
            .builder()
            .into(),
        ))]);

        assert_parse_chumsky(parm_list_expr, &ast_expected, &parm_list_chumsky);
    }

    #[test]
    fn parm_list_two_elems_one_eval_one_field() {
        let parm_list_expr = "foo.bar(), baz.quux)";

        let ast_expected = Some(vec![
            ParmElem::TernaryExpression(Box::new(
                Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("foo".to_owned()),
                        "bar".to_owned(),
                    ),
                    parm_list: None,
                })
                .builder()
                .into(),
            )),
            ParmElem::TernaryExpression(Box::new(
                Factor::Field(Field::QualifierId(
                    Qualifier::Id("baz".to_owned()),
                    "quux".to_owned(),
                ))
                .builder()
                .into(),
            )),
        ]);

        assert_parse_chumsky(parm_list_expr, &ast_expected, &parm_list_chumsky);
    }

    #[test]
    fn parm_list_one_elem_nested() {
        let parm_list_expr = "foo.bar(baz))";

        let ast_expected = Some(vec![ParmElem::TernaryExpression(Box::new(
            Factor::Eval(Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("foo".to_owned()), "bar".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("baz".to_owned())).builder().into(),
                ))]),
            })
            .builder()
            .into(),
        ))]);

        assert_parse_chumsky(parm_list_expr, &ast_expected, &parm_list_chumsky);
    }

    #[test]
    fn parm_list_one_elem_nested_eval() {
        let parm_list_expr = "foo.bar(baz.quux()))";

        let ast_expected = Some(vec![ParmElem::TernaryExpression(Box::new(
            Factor::Eval(Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("foo".to_owned()), "bar".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("baz".to_owned()),
                            "quux".to_owned(),
                        ),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                ))]),
            })
            .builder()
            .into(),
        ))]);

        assert_parse_chumsky(parm_list_expr, &ast_expected, &parm_list_chumsky);
    }
}
