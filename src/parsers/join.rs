use chumsky::{prelude::*, text::whitespace};
use dracaena::ast::join::{JoinClause, JoinList, JoinOrder, JoinSpec, JoinUsing, JoinVariant};

use super::{
    common::keyword_no_case, index_hint::index_hint_chumsky, order_group::order_group_chumsky,
    select_option::select_option_chumsky, table::table_chumsky,
    ternary_expression::ternary_expr_chumsky,
};

fn join_variant_chumsky<'a>(
) -> impl Parser<'a, &'a str, JoinVariant, extra::Err<Rich<'a, char>>> + Clone {
    choice((
        keyword_no_case("outer").to(JoinVariant::Outer),
        keyword_no_case("exists").to(JoinVariant::Exists),
        keyword_no_case("notexists").to(JoinVariant::NotExists),
    ))
}

fn join_clause_chumsky<'a>(
) -> impl Parser<'a, &'a str, JoinClause, extra::Err<Rich<'a, char>>> + Clone {
    join_variant_chumsky()
        .then_ignore(whitespace().at_least(1))
        .or_not()
        .then_ignore(keyword_no_case("join"))
        .then_ignore(whitespace().at_least(1))
        .then(
            select_option_chumsky()
                .then_ignore(whitespace().at_least(1))
                .or_not(),
        )
        .then(table_chumsky())
        .map(|((join_variant, select_opts), table)| JoinClause {
            join_variant,
            // TODO: check, if this is still true with `chumsky` rewrite
            // FIXME: it is weird: select_option always has Some(...) even, when we have no select option parsed;
            // that's why we have to use this hack here
            select_opts: select_opts.filter(|so| !so.is_empty()),
            table,
        })
}

fn join_using_chumsky<'a>(
) -> impl Parser<'a, &'a str, JoinUsing, extra::Err<Rich<'a, char>>> + Clone {
    join_clause_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(index_hint_chumsky())
                .or_not(),
        )
        .map(|(join_clause, index_hint)| JoinUsing {
            join_clause,
            index_hint,
        })
}

fn join_order_chumsky<'a>(
) -> impl Parser<'a, &'a str, JoinOrder, extra::Err<Rich<'a, char>>> + Clone {
    join_using_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(order_group_chumsky())
                .or_not(),
        )
        .map(|(join_using, order_group)| JoinOrder {
            join_using,
            order_group,
        })
}

fn join_spec_chumsky<'a>() -> impl Parser<'a, &'a str, JoinSpec, extra::Err<Rich<'a, char>>> + Clone
{
    join_order_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(keyword_no_case("where"))
                .ignore_then(whitespace().at_least(1))
                .ignore_then(ternary_expr_chumsky())
                .or_not(),
        )
        .map(|(join_order, if_expr)| JoinSpec {
            join_order,
            if_expr,
        })
}

pub fn join_list_chumsky<'a>(
) -> impl Parser<'a, &'a str, JoinList, extra::Err<Rich<'a, char>>> + Clone {
    join_spec_chumsky()
        .separated_by(whitespace().at_least(1))
        .collect()
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    use dracaena::ast::column::Column;
    use dracaena::ast::constant::Constant;
    use dracaena::ast::expression::{
        ComparisonExpression, ConditionalExpression, ConditionalExpressionData, Expression,
        SimpleExpression, TernaryExpression,
    };
    use dracaena::ast::factor::*;
    use dracaena::ast::field::Field;
    use dracaena::ast::field_column::*;
    use dracaena::ast::from::From;

    use dracaena::ast::operator::*;
    use dracaena::ast::order_elem::*;
    use dracaena::ast::order_group::*;
    use dracaena::ast::qualifier::Qualifier;
    use dracaena::ast::select_option::*;
    use dracaena::ast::table::*;
    use dracaena::ast::term::*;

    #[test]
    fn join_list_join_table() {
        let join_clause = "join custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_exists_join_table() {
        let join_clause = "exists join custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Exists),
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_not_exists_join_table() {
        let join_clause = "notExists join custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::NotExists),
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_outer_join_table() {
        let join_clause = "outer join custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Outer),
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_variant_with_select_opts_table() {
        let join_clause = "outer join firstonly firstFast custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Outer),
                        select_opts: Some(vec![
                            SelectOpts::FirstOnly(FirstOnly::FirstOnly),
                            SelectOpts::FirstFast,
                        ]),
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_fields_from_table() {
        let join_clause = "join foo, bar from custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::WithFields(TableWithFields {
                            fields: vec![
                                FieldList::Col(Column {
                                    name: "foo".to_owned(),
                                    function: None,
                                    index: None,
                                }),
                                FieldList::Col(Column {
                                    name: "bar".to_owned(),
                                    function: None,
                                    index: None,
                                }),
                            ],
                            from: From {
                                table: "custTable".to_owned(),
                            },
                        }),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_all_fields_from_table() {
        let join_clause = "join * from custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::WithFields(TableWithFields {
                            fields: vec![FieldList::All],
                            from: From {
                                table: "custTable".to_owned(),
                            },
                        }),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_table_with_select_ops() {
        let join_clause = "join firstOnly custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: Some(vec![SelectOpts::FirstOnly(FirstOnly::FirstOnly)]),
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_variant_join_fields_from_table() {
        let join_clause = "exists join foo, bar from custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Exists),
                        select_opts: None,
                        table: Table::WithFields(TableWithFields {
                            fields: vec![
                                FieldList::Col(Column {
                                    name: "foo".to_owned(),
                                    function: None,
                                    index: None,
                                }),
                                FieldList::Col(Column {
                                    name: "bar".to_owned(),
                                    function: None,
                                    index: None,
                                }),
                            ],
                            from: From {
                                table: "custTable".to_owned(),
                            },
                        }),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_variant_join_all_fields_from_table() {
        let join_clause = "exists join * from custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Exists),
                        select_opts: None,
                        table: Table::WithFields(TableWithFields {
                            fields: vec![FieldList::All],
                            from: From {
                                table: "custTable".to_owned(),
                            },
                        }),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_variant_join_fields_with_select_opts_from_table() {
        let join_clause = "exists join firstonly firstFast foo, bar from custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Exists),
                        select_opts: Some(vec![
                            SelectOpts::FirstOnly(FirstOnly::FirstOnly),
                            SelectOpts::FirstFast,
                        ]),
                        table: Table::WithFields(TableWithFields {
                            fields: vec![
                                FieldList::Col(Column {
                                    name: "foo".to_owned(),
                                    function: None,
                                    index: None,
                                }),
                                FieldList::Col(Column {
                                    name: "bar".to_owned(),
                                    function: None,
                                    index: None,
                                }),
                            ],
                            from: From {
                                table: "custTable".to_owned(),
                            },
                        }),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_variant_join_all_fields_with_select_opts_from_table() {
        let join_clause = "exists join firstonly firstFast * from custTable";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: Some(JoinVariant::Exists),
                        select_opts: Some(vec![
                            SelectOpts::FirstOnly(FirstOnly::FirstOnly),
                            SelectOpts::FirstFast,
                        ]),
                        table: Table::WithFields(TableWithFields {
                            fields: vec![FieldList::All],
                            from: From {
                                table: "custTable".to_owned(),
                            },
                        }),
                    },
                    index_hint: None,
                },
                order_group: None,
            },
            if_expr: None,
        }];

        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_table_with_order_group() {
        let join_clause = "join custTable group by custTable.foo";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: Some(OrderGroupData {
                    order_group: OrderGroup::GroupBy(GroupOrderBy {
                        by: true,
                        order_elems: vec![OrderElem {
                            table: Some("custTable".to_owned()),
                            field: "foo".to_owned(),
                            index: None,
                            direction: None,
                        }],
                    }),
                    other_order_group: None,
                }),
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_table_with_order_group_not_qualified() {
        let join_clause = "join custTable group by foo";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: Some(OrderGroupData {
                    order_group: OrderGroup::GroupBy(GroupOrderBy {
                        by: true,
                        order_elems: vec![OrderElem {
                            table: None,
                            field: "foo".to_owned(),
                            index: None,
                            direction: None,
                        }],
                    }),
                    other_order_group: None,
                }),
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_table_with_order_group_and_order() {
        let join_clause = "join custTable group by custTable.foo desc";
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: Some(OrderGroupData {
                    order_group: OrderGroup::GroupBy(GroupOrderBy {
                        by: true,
                        order_elems: vec![OrderElem {
                            table: Some("custTable".to_owned()),
                            field: "foo".to_owned(),
                            index: None,
                            direction: Some(Direction::Descending),
                        }],
                    }),
                    other_order_group: None,
                }),
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_table_with_both_order_groups() {
        let join_clause = r#"join custTable
            group by custTable.foo
            order by custTable.bar"#;
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: Some(OrderGroupData {
                    order_group: OrderGroup::GroupBy(GroupOrderBy {
                        by: true,
                        order_elems: vec![OrderElem {
                            table: Some("custTable".to_owned()),
                            field: "foo".to_owned(),
                            index: None,
                            direction: None,
                        }],
                    }),
                    other_order_group: Some(OrderGroup::OrderBy(GroupOrderBy {
                        by: true,
                        order_elems: vec![OrderElem {
                            table: Some("custTable".to_owned()),
                            field: "bar".to_owned(),
                            index: None,
                            direction: None,
                        }],
                    })),
                }),
            },
            if_expr: None,
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_join_table_with_order_group_and_where() {
        let join_clause = r#"join custTable
            group by custTable.Foo
            where (custTable.Foo == 'bar' && custTable.Party == dirParty.RecId)"#;
        let ast_expected = vec![JoinSpec {
            join_order: JoinOrder {
                join_using: JoinUsing {
                    join_clause: JoinClause {
                        join_variant: None,
                        select_opts: None,
                        table: Table::Standalone("custTable".to_owned()),
                    },
                    index_hint: None,
                },
                order_group: Some(OrderGroupData {
                    order_group: OrderGroup::GroupBy(GroupOrderBy {
                        by: true,
                        order_elems: vec![OrderElem {
                            table: Some("custTable".to_owned()),
                            field: "Foo".to_owned(),
                            index: None,
                            direction: None,
                        }],
                    }),
                    other_order_group: None,
                }),
            },
            if_expr: Some(
                TernaryExpression::ConditionalExpression(
                    ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
                        cond_expr_left: Box::new(ConditionalExpression::Expression(
                            Expression::Comparison(ComparisonExpression {
                                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("custTable".to_owned()),
                                            "Foo".to_owned(),
                                        )),
                                    }),
                                )),
                                comp_op: ComparisonOperator::Equal,
                                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Constant(Constant::String(
                                            "bar".to_owned(),
                                        )),
                                    }),
                                )),
                            }),
                        )),
                        log_op: LogicalOperator::And,
                        cond_expr_right: Box::new(ConditionalExpression::Expression(
                            Expression::Comparison(ComparisonExpression {
                                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("custTable".to_owned()),
                                            "Party".to_owned(),
                                        )),
                                    }),
                                )),
                                comp_op: ComparisonOperator::Equal,
                                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("dirParty".to_owned()),
                                            "RecId".to_owned(),
                                        )),
                                    }),
                                )),
                            }),
                        )),
                    }),
                )
                .parenthesize(),
            ),
        }];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }

    #[test]
    fn join_list_multiple_join_with_multiple_order_group_and_where() {
        let join_clause = r#"join custTable
            group by custTable.Foo
            join dirParty
                order by Name asc, Age desc
                where custTable.Foo == dirParty.Name"#;
        let ast_expected = vec![
            JoinSpec {
                join_order: JoinOrder {
                    join_using: JoinUsing {
                        join_clause: JoinClause {
                            join_variant: None,
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        },
                        index_hint: None,
                    },
                    order_group: Some(OrderGroupData {
                        order_group: OrderGroup::GroupBy(GroupOrderBy {
                            by: true,
                            order_elems: vec![OrderElem {
                                table: Some("custTable".to_owned()),
                                field: "Foo".to_owned(),
                                index: None,
                                direction: None,
                            }],
                        }),
                        other_order_group: None,
                    }),
                },
                if_expr: None,
            },
            JoinSpec {
                join_order: JoinOrder {
                    join_using: JoinUsing {
                        join_clause: JoinClause {
                            join_variant: None,
                            select_opts: None,
                            table: Table::Standalone("dirParty".to_owned()),
                        },
                        index_hint: None,
                    },
                    order_group: Some(OrderGroupData {
                        order_group: OrderGroup::OrderBy(GroupOrderBy {
                            by: true,
                            order_elems: vec![
                                OrderElem {
                                    table: None,
                                    field: "Name".to_owned(),
                                    index: None,
                                    direction: Some(Direction::Ascending),
                                },
                                OrderElem {
                                    table: None,
                                    field: "Age".to_owned(),
                                    index: None,
                                    direction: Some(Direction::Descending),
                                },
                            ],
                        }),
                        other_order_group: None,
                    }),
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Foo".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("dirParty".to_owned()),
                                        "Name".to_owned(),
                                    )),
                                }),
                            )),
                        },
                    )),
                )),
            },
        ];
        assert_parse_chumsky(join_clause, &ast_expected, &join_list_chumsky);
    }
}
