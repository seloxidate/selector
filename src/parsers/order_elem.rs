use chumsky::error::Rich;
use chumsky::extra;
use chumsky::primitive::just;
use chumsky::text::whitespace;
use chumsky::Parser;
use dracaena::ast::order_elem::*;

use std::str;

use super::common::column_idx_chumsky;
use super::common::keyword_no_case;
use super::common::xpp_identifier_chumsky;

pub fn order_elem_chumsky<'a>(
) -> impl Parser<'a, &'a str, OrderElem, extra::Err<Rich<'a, char>>> + Clone {
    let table_qualifier_opt = xpp_identifier_chumsky().then_ignore(just('.')).or_not();
    let dir_opt = whitespace()
        .at_least(1)
        .ignore_then(keyword_no_case("asc"))
        .to(Direction::Ascending)
        .or(whitespace()
            .at_least(1)
            .ignore_then(keyword_no_case("desc"))
            .to(Direction::Descending))
        .or_not();
    table_qualifier_opt
        .then(xpp_identifier_chumsky())
        .then(column_idx_chumsky().or_not())
        .then(dir_opt)
        .map(|(((table_name, field_name), index), direction)| OrderElem {
            table: table_name.map(|s| s.to_owned()),
            field: field_name.to_owned(),
            index,
            direction,
        })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    #[test]
    fn order_elem_only_field() {
        assert_parse_chumsky(
            "foo",
            &OrderElem {
                field: "foo".to_owned(),
                table: None,
                direction: None,
                index: None,
            },
            &order_elem_chumsky,
        );
    }

    #[test]
    fn order_elem_field_and_index() {
        assert_parse_chumsky(
            "foo[42]",
            &OrderElem {
                field: "foo".to_owned(),
                table: None,
                direction: None,
                index: Some(42),
            },
            &order_elem_chumsky,
        );
    }

    #[test]
    fn order_elem_field_and_index_and_direction() {
        assert_parse_chumsky(
            "foo[42] desc",
            &OrderElem {
                field: "foo".to_owned(),
                table: None,
                direction: Some(Direction::Descending),
                index: Some(42),
            },
            &order_elem_chumsky,
        );
    }

    #[test]
    fn order_elem_with_table_unknown_func_err() {
        let stmt_that_fails = "mytable.foo(recId)";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }

    #[test]
    fn order_elem_with_table_and_field_but_second_field_err() {
        let stmt_that_fails = "mytable.foo, bar";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }

    #[test]
    fn order_elem_with_table_and_direction_but_second_field_err() {
        let stmt_that_fails = "mytable.foo desc, bar";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }

    #[test]
    fn order_elem_with_table_and_index_but_second_field_err() {
        let stmt_that_fails = "mytable.foo[42], bar";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }

    #[test]
    fn order_elem_with_table_and_index_and_direction_but_second_field_err() {
        let stmt_that_fails = "mytable.foo[42] asc, bar";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }

    #[test]
    fn order_elem_with_table_and_both_directions_err() {
        let stmt_that_fails = "mytable.foo asc desc";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }

    #[test]
    fn order_elem_terminated_by_whitespace_err() {
        let stmt_that_fails = "foo ";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());

        let stmt_that_fails = "mytable.foo ";

        assert!(order_elem_chumsky().parse(stmt_that_fails).has_errors());
    }
}
