use chumsky::error::Rich;
use chumsky::Parser;
use chumsky::{extra, text::whitespace};
use dracaena::ast::from::From;

use std::str;

use super::common::{keyword_no_case, xpp_identifier_chumsky};

pub fn from_chumsky<'a>() -> impl Parser<'a, &'a str, From, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case("from")
        .then_ignore(whitespace().at_least(1))
        .then(xpp_identifier_chumsky())
        .map(|(_, table_name)| From {
            table: table_name.to_string(),
        })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    #[test]
    fn from_success() {
        let from_str = "from foo";

        let expected = From {
            table: "foo".to_owned(),
        };

        assert_parse_chumsky(from_str, &expected, &from_chumsky);
    }

    #[test]
    fn from_opt_is_some_when_lower_and_uppercase() {
        let from_str = "FrOm foo";

        let expected = From {
            table: "foo".to_owned(),
        };

        assert_parse_chumsky(from_str, &expected, &from_chumsky);
    }

    #[test]
    fn from_opt_is_none() {
        let from_err = "select custTable where";

        assert!(from_chumsky().parse(from_err).has_errors());
    }

    #[test]
    fn from_opt_is_none_when_no_space() {
        let str_to_err = "fromCustTable";

        assert!(from_chumsky().parse(str_to_err).has_errors());
    }
}
