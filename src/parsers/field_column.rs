use chumsky::{error::Rich, extra, primitive::just, IterParser, Parser};
use dracaena::ast::{field_column::FieldList, order_elem::*};

use std::str;

use super::{column::column_in_select_chumsky, order_elem::order_elem_chumsky};

pub fn field_list_chumsky<'a>(
) -> impl Parser<'a, &'a str, Vec<FieldList>, extra::Err<Rich<'a, char>>> + Clone {
    just('*')
        .to(vec![FieldList::All])
        .or(column_in_select_chumsky()
            .map(FieldList::Col)
            .separated_by(just(',').padded())
            .collect::<Vec<_>>())
}

pub fn fields_in_order_group_chumsky<'a>(
) -> impl Parser<'a, &'a str, Vec<OrderElem>, extra::Err<Rich<'a, char>>> + Clone {
    order_elem_chumsky()
        .separated_by(just(',').padded())
        .collect()
}

#[cfg(test)]
mod tests {
    use dracaena::ast::column::Column;

    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    #[test]
    fn field_list_all() {
        let column_name = "*";

        let parsed_expected = vec![FieldList::All];

        assert_parse_chumsky(column_name, &parsed_expected, &field_list_chumsky);
    }

    #[test]
    fn fields_in_option_all_at_start_should_err() {
        let stmt_that_fails = "* asc";

        assert!(fields_in_order_group_chumsky()
            .parse(stmt_that_fails)
            .has_errors());
    }

    #[test]
    fn field_list_and_order_elem_without_table() {
        let field_list_str = "foo, bar";

        let parsed_expected = vec![
            FieldList::Col(Column {
                name: "foo".to_owned(),
                function: None,
                index: None,
            }),
            FieldList::Col(Column {
                name: "bar".to_owned(),
                function: None,
                index: None,
            }),
        ];

        let parsed_expected_order_elem: Vec<_> = parsed_expected
            .iter()
            .map(get_column_from_field_list)
            .map(OrderElem::from)
            .collect();

        assert_parse_chumsky(field_list_str, &parsed_expected, &field_list_chumsky);

        assert_parse_chumsky(
            field_list_str,
            &parsed_expected_order_elem,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn fields_in_order_group_with_table() {
        let field_list_str = "foo, myTable.bar";

        let parsed_expected = vec![
            OrderElem {
                field: "foo".to_owned(),
                table: None,
                index: None,
                direction: None,
            },
            OrderElem {
                field: "bar".to_owned(),
                table: Some("myTable".to_owned()),
                index: None,
                direction: None,
            },
        ];

        assert_parse_chumsky(
            field_list_str,
            &parsed_expected,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn fields_in_order_group_with_table_one_ascending_one_descending() {
        let field_list_str = "foo asc, myTable.bar desc";

        let parsed_expected = vec![
            OrderElem {
                field: "foo".to_owned(),
                table: None,
                index: None,
                direction: Some(Direction::Ascending),
            },
            OrderElem {
                field: "bar".to_owned(),
                table: Some("myTable".to_owned()),
                index: None,
                direction: Some(Direction::Descending),
            },
        ];

        assert_parse_chumsky(
            field_list_str,
            &parsed_expected,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn fields_in_order_group_with_table_and_direction_spaces_everywhere() {
        let field_list_str = "foo   asc    ,   myTable.bar    desc";

        let parsed_expected = vec![
            OrderElem {
                field: "foo".to_owned(),
                table: None,
                index: None,
                direction: Some(Direction::Ascending),
            },
            OrderElem {
                field: "bar".to_owned(),
                table: Some("myTable".to_owned()),
                index: None,
                direction: Some(Direction::Descending),
            },
        ];

        assert_parse_chumsky(
            field_list_str,
            &parsed_expected,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn fields_in_order_group_with_table_index_and_direction_spaces_everywhere() {
        let field_list_str = "foo[42]   asc    ,   myTable.bar    desc";

        let parsed_expected = vec![
            OrderElem {
                field: "foo".to_owned(),
                table: None,
                index: Some(42),
                direction: Some(Direction::Ascending),
            },
            OrderElem {
                field: "bar".to_owned(),
                table: Some("myTable".to_owned()),
                index: None,
                direction: Some(Direction::Descending),
            },
        ];

        assert_parse_chumsky(
            field_list_str,
            &parsed_expected,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn field_list_and_field_option_spaces_everywhere() {
        let column_names = "foo , bar  ,baz";
        let parsed_expected = vec![
            FieldList::Col(Column {
                name: "foo".to_owned(),
                function: None,
                index: None,
            }),
            FieldList::Col(Column {
                name: "bar".to_owned(),
                function: None,
                index: None,
            }),
            FieldList::Col(Column {
                name: "baz".to_owned(),
                function: None,
                index: None,
            }),
        ];

        let parsed_expected_order_elem: Vec<_> = parsed_expected
            .iter()
            .map(get_column_from_field_list)
            .map(OrderElem::from)
            .collect();

        assert_parse_chumsky(column_names, &parsed_expected, &field_list_chumsky);
        assert_parse_chumsky(
            column_names,
            &parsed_expected_order_elem,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn field_list_and_field_option_with_index_spaces_everywhere() {
        let column_names = "foo[42] , bar[1337]  , baz";
        let parsed_expected = vec![
            FieldList::Col(Column {
                name: "foo".to_owned(),
                function: None,
                index: Some(42),
            }),
            FieldList::Col(Column {
                name: "bar".to_owned(),
                function: None,
                index: Some(1337),
            }),
            FieldList::Col(Column {
                name: "baz".to_owned(),
                function: None,
                index: None,
            }),
        ];

        let parsed_expected_order_elem: Vec<_> = parsed_expected
            .iter()
            .map(get_column_from_field_list)
            .map(OrderElem::from)
            .collect();

        assert_parse_chumsky(column_names, &parsed_expected, &field_list_chumsky);
        assert_parse_chumsky(
            column_names,
            &parsed_expected_order_elem,
            &fields_in_order_group_chumsky,
        );
    }

    #[test]
    fn field_list_all_at_start_of_some_fields_should_err() {
        let column_names = "*, foo, bar";

        assert!(field_list_chumsky().parse(column_names).has_errors());
    }

    #[test]
    fn field_option_all_at_start_of_some_fields_should_err() {
        let column_names = "*, foo, bar";

        assert!(fields_in_order_group_chumsky()
            .parse(column_names)
            .has_errors());
    }

    #[test]
    fn field_list_and_field_option_all_in_middle_of_some_fields_should_err() {
        let column_names = "foo, *, bar";

        assert!(field_list_chumsky().parse(column_names).has_errors());

        assert!(fields_in_order_group_chumsky()
            .parse(column_names)
            .has_errors());
    }

    #[test]
    fn field_list_and_field_option_all_at_end_of_some_fields_should_err() {
        let column_names = "foo, bar, *";

        assert!(field_list_chumsky().parse(column_names).has_errors());

        assert!(fields_in_order_group_chumsky()
            .parse(column_names)
            .has_errors());
    }

    #[test]
    fn field_list_table_prefix_should_err() {
        let column_names = "foo.bar, zaz";

        assert!(field_list_chumsky().parse(column_names).has_errors());
    }

    #[test]
    fn fields_in_options_table_prefix() {
        let column_names = "foo.bar, zaz";

        let parsed_expected = vec![
            OrderElem {
                field: "bar".to_owned(),
                table: Some("foo".to_owned()),
                index: None,
                direction: None,
            },
            OrderElem {
                field: "zaz".to_owned(),
                table: None,
                index: None,
                direction: None,
            },
        ];

        assert_parse_chumsky(
            column_names,
            &parsed_expected,
            &fields_in_order_group_chumsky,
        );
    }

    /// You are not allowed to use function expressions in field option list.
    #[test]
    fn fields_in_options_table_with_func_expr_only_consume_until_paren() {
        let column_names = "count(foo.bar), avg(quux.zaz),";

        assert!(fields_in_order_group_chumsky()
            .parse(column_names)
            .has_errors());
    }

    fn get_column_from_field_list(field_list: &FieldList) -> &Column {
        match field_list {
            FieldList::Col(col) => col,
            _ => panic!("Wrong use of test"),
        }
    }
}
