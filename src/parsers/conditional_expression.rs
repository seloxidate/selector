use chumsky::error::Rich;
use chumsky::extra;
use chumsky::primitive::choice;
use chumsky::primitive::just;
use chumsky::primitive::recurse;
use chumsky::recursive::recursive;
use chumsky::text::whitespace;
use chumsky::Parser;

use dracaena::ast::expression::{
    ComparisonExpression, ConditionalExpression, ConditionalExpressionData, Expression,
    SimpleExpression, SimpleExpressionData,
};
use dracaena::ast::factor::*;
use dracaena::ast::operator::*;
use dracaena::ast::term::*;

use std::str;

use super::common::keyword_no_case;
use super::constant::constant_chumsky;
use super::eval::eval_chumsky;
use super::field::field_chumsky;
use super::ternary_expression::ternary_expr_chumsky;

fn comparison_operator_chumsky<'a>(
) -> impl Parser<'a, &'a str, ComparisonOperator, extra::Err<Rich<'a, char>>> + Clone {
    choice((
        just("==").to(ComparisonOperator::Equal),
        just("!=").to(ComparisonOperator::NotEqual),
        just(">=").to(ComparisonOperator::GreaterEqual),
        just("<=").to(ComparisonOperator::LessEqual),
        just(">").to(ComparisonOperator::Greater),
        just("<").to(ComparisonOperator::Less),
    ))
}

fn logical_operator_chumsky<'a>(
) -> impl Parser<'a, &'a str, LogicalOperator, extra::Err<Rich<'a, char>>> + Clone {
    just("||")
        .to(LogicalOperator::Or)
        .or(just("&&").to(LogicalOperator::And))
}

pub fn conditional_expr_chumsky<'a>(
) -> impl Parser<'a, &'a str, ConditionalExpression, extra::Err<Rich<'a, char>>> + Clone {
    expression_chumsky()
        .then(cond_expr_right_recursive_chumsky().or_not())
        .map(|(expr, cond_expr_rr)| {
            if let Some(crr) = cond_expr_rr {
                crr.into_conditional_expression(ConditionalExpression::Expression(expr))
            } else {
                ConditionalExpression::Expression(expr)
            }
        })
}

fn cond_expr_right_recursive_chumsky<'a>(
) -> impl Parser<'a, &'a str, ConditionalExpressionRightRecursive, extra::Err<Rich<'a, char>>> + Clone
{
    recursive(|tree| {
        logical_operator_chumsky()
            .padded()
            .then_ignore(whitespace())
            .then(recurse(expression_chumsky))
            .then(tree.or_not())
            .map(
                |((log_op, expr), cond_expr_rr)| ConditionalExpressionRightRecursive {
                    log_op,
                    expression: expr,
                    cond_expr_recursive: cond_expr_rr.map(Box::new),
                },
            )
    })
}

struct ConditionalExpressionRightRecursive {
    log_op: LogicalOperator,
    expression: Expression,
    cond_expr_recursive: Option<Box<ConditionalExpressionRightRecursive>>,
}

impl ConditionalExpressionRightRecursive {
    pub fn into_conditional_expression(
        self,
        cond_expr: ConditionalExpression,
    ) -> ConditionalExpression {
        let first = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(cond_expr),
            log_op: self.log_op,
            cond_expr_right: Box::new(ConditionalExpression::Expression(self.expression)),
        });
        if let Some(cond_expr_rr) = self.cond_expr_recursive {
            cond_expr_rr.into_conditional_expression(first)
        } else {
            first
        }
    }
}

fn expression_chumsky<'a>(
) -> impl Parser<'a, &'a str, Expression, extra::Err<Rich<'a, char>>> + Clone {
    comp_expr_chumsky()
        .map(Expression::Comparison)
        .or(recurse(simple_expr_chumsky).map(Expression::Simple))
    // TODO: implement other variants as well
}

fn comp_expr_chumsky<'a>(
) -> impl Parser<'a, &'a str, ComparisonExpression, extra::Err<Rich<'a, char>>> + Clone {
    recurse(simple_expr_chumsky)
        .then(comparison_operator_chumsky().padded())
        .then(recurse(simple_expr_chumsky))
        .map(|((left, op), right)| ComparisonExpression {
            simple_expr_left: left,
            comp_op: op,
            simple_expr_right: right,
        })
}

fn simple_expr_chumsky<'a>(
) -> impl Parser<'a, &'a str, SimpleExpression, extra::Err<Rich<'a, char>>> + Clone {
    recurse(simple_expr_data_chumsky)
        .map(SimpleExpression::SimpleExpression)
        .or(term_chumsky().map(SimpleExpression::Term))
}

// FIXME: this is left recursive, so we end up in infinte recursion!

fn simple_expr_data_chumsky<'a>(
) -> impl Parser<'a, &'a str, SimpleExpressionData, extra::Err<Rich<'a, char>>> + Clone {
    term_chumsky()
        .then(add_op_chumsky().padded())
        .then(recurse(simple_expr_chumsky))
        .map(|((term, add_op), simple_expr)| SimpleExpressionData {
            simple_expr: Box::new(simple_expr),
            add_op,
            term,
        })
        // we have indirect recursion, so we need to `Box` it
        .boxed()
}

fn add_op_chumsky<'a>() -> impl Parser<'a, &'a str, AddOperator, extra::Err<Rich<'a, char>>> + Clone
{
    choice((
        just('-').to(AddOperator::Minus),
        just('|').to(AddOperator::PhysicalOr),
        just('+').to(AddOperator::Plus),
    ))
}

fn mul_op_chumsky<'a>(
) -> impl Parser<'a, &'a str, MultiplyOperator, extra::Err<Rich<'a, char>>> + Clone {
    choice((
        just('/').to(MultiplyOperator::Div),
        keyword_no_case("div").to(MultiplyOperator::DivInt),
        keyword_no_case("mod").to(MultiplyOperator::Mod),
        just('*').to(MultiplyOperator::Mult),
        just('&').to(MultiplyOperator::PhysicalAnd),
        just('^').to(MultiplyOperator::PhysicalXOr),
        just("<<").to(MultiplyOperator::ShiftLeft),
        just(">>").to(MultiplyOperator::ShiftRight),
    ))
}

fn term_chumsky<'a>() -> impl Parser<'a, &'a str, Term, extra::Err<Rich<'a, char>>> + Clone {
    complement_fact_chumsky()
        .then(term_data_right_recursive_chumsky().or_not())
        .map(|(cmpl_fact, term_rr)| {
            let term_cmpl_fact = Term::ComplementFactor(cmpl_fact);
            if let Some(trr) = term_rr {
                trr.into_term(term_cmpl_fact)
            } else {
                term_cmpl_fact
            }
        })
}

fn term_data_right_recursive_chumsky<'a>(
) -> impl Parser<'a, &'a str, TermDataRightRecursive, extra::Err<Rich<'a, char>>> + Clone {
    recursive(|tree| {
        mul_op_chumsky()
            .padded()
            .then(complement_fact_chumsky())
            .then(tree.or_not())
            .map(|((mul_op, cmpl_fact), term_rr)| TermDataRightRecursive {
                mul_op,
                cmpl_fact,
                term_data_recursive: term_rr.map(Box::new),
            })
    })
}

struct TermDataRightRecursive {
    mul_op: MultiplyOperator,
    cmpl_fact: ComplementFactor,
    term_data_recursive: Option<Box<TermDataRightRecursive>>,
}

impl TermDataRightRecursive {
    pub fn into_term(self, term: Term) -> Term {
        let first = Term::Term(TermData {
            term: Box::new(term),
            mul_op: self.mul_op,
            cmpl_fact: self.cmpl_fact,
        });
        if let Some(term_data_rr) = self.term_data_recursive {
            term_data_rr.into_term(first)
        } else {
            first
        }
    }
}

fn complement_fact_chumsky<'a>(
) -> impl Parser<'a, &'a str, ComplementFactor, extra::Err<Rich<'a, char>>> + Clone {
    just('!')
        .ignore_then(whitespace())
        .ignore_then(second_fact_chumsky())
        .map(ComplementFactor::NotSecondFactor)
        .or(second_fact_chumsky().map(ComplementFactor::SecondFactor))
}

fn second_fact_chumsky<'a>(
) -> impl Parser<'a, &'a str, SecondFactor, extra::Err<Rich<'a, char>>> + Clone {
    sign_op_chumsky()
        .then_ignore(whitespace())
        .or_not()
        .then(factor_chumsky())
        .map(|(sign_op, factor)| SecondFactor { sign_op, factor })
}

fn sign_op_chumsky<'a>(
) -> impl Parser<'a, &'a str, SignOperator, extra::Err<Rich<'a, char>>> + Clone {
    just('-')
        .to(SignOperator::Minus)
        .or(just('~').to(SignOperator::PhysicalNot))
}

// TODO: implement missing alternatives

fn factor_chumsky<'a>() -> impl Parser<'a, &'a str, Factor, extra::Err<Rich<'a, char>>> + Clone {
    // TODO: this is only dummy right now in order to compile
    choice((
        recurse(eval_chumsky).map(Factor::Eval),
        recurse(ternary_expr_chumsky)
            .padded()
            .delimited_by(just('('), just(')'))
            .map(|inner| Factor::ParenthesizedTernary(Box::new(inner))),
        constant_chumsky().map(Factor::Constant),
        recurse(field_chumsky).map(Factor::Field),
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::constant::Constant;

    use dracaena::ast::expression::TernaryExpression;
    use dracaena::ast::field::Field;
    use dracaena::ast::qualifier::Qualifier;

    #[test]
    fn variable_expr_not() {
        let expr_str = "!myBooleanVar";

        let expected =
            ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                Term::ComplementFactor(ComplementFactor::NotSecondFactor(SecondFactor {
                    sign_op: None,
                    factor: Factor::Field(Field::Id("myBooleanVar".to_owned())),
                })),
            )));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    fn create_factor_paren_ternary(
        is_true: bool,
        tern_expr: TernaryExpression,
    ) -> ConditionalExpression {
        ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
            Term::ComplementFactor(if !is_true {
                ComplementFactor::NotSecondFactor(SecondFactor {
                    sign_op: None,
                    factor: Factor::ParenthesizedTernary(Box::new(tern_expr)),
                })
            } else {
                ComplementFactor::SecondFactor(SecondFactor {
                    sign_op: None,
                    factor: Factor::ParenthesizedTernary(Box::new(tern_expr)),
                })
            }),
        )))
    }

    #[test]
    fn variable_expr_parens() {
        let expr_str = "(myBool)";
        let expected = create_factor_paren_ternary(
            true,
            TernaryExpression::ConditionalExpression(ConditionalExpression::Expression(
                Expression::Simple(SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("myBool".to_owned())),
                    }),
                ))),
            )),
        );

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn variable_expr_not_parens() {
        let expr_str = "!(myBool)";
        let expected = create_factor_paren_ternary(
            false,
            TernaryExpression::ConditionalExpression(ConditionalExpression::Expression(
                Expression::Simple(SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("myBool".to_owned())),
                    }),
                ))),
            )),
        );

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn variable_expr_not_parens_not() {
        let expr_str = "!(!myBool)";
        let expected = create_factor_paren_ternary(
            false,
            TernaryExpression::ConditionalExpression(ConditionalExpression::Expression(
                Expression::Simple(SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::NotSecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("myBool".to_owned())),
                    }),
                ))),
            )),
        );

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn simple_expr_literal_single_quoted() {
        let expr_str = "'some Literal'";

        let expected =
            ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                    sign_op: None,
                    factor: Factor::Constant(Constant::String("some Literal".to_owned())),
                })),
            )));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn simple_expr_literal_double_quoted() {
        let expr_str = r#""some Literal""#;

        let expected =
            ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                    sign_op: None,
                    factor: Factor::Constant(Constant::String("some Literal".to_owned())),
                })),
            )));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn simple_expr_column() {
        let expr_str = "myTable.myField";

        let expected =
            ConditionalExpression::Expression(Expression::Simple(SimpleExpression::Term(
                Term::ComplementFactor(ComplementFactor::SecondFactor(SecondFactor {
                    sign_op: None,
                    factor: Factor::Field(Field::QualifierId(
                        Qualifier::Id("myTable".to_owned()),
                        "myField".to_owned(),
                    )),
                })),
            )));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_simple_compare() {
        let expr_str = "foo == bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::Equal,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_with_literal() {
        let expr_str = "foo == 'bar'";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::Equal,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Constant(Constant::String("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    /// This test is pretty important, as it shows that '<=' has prio over '<'
    #[test]
    fn boolean_primary_compare_less_equal() {
        let expr_str = "foo <= bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::LessEqual,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    /// This test is pretty important, as it shows that '>=' has prio over '>'
    #[test]
    fn boolean_primary_compare_greater_equal() {
        let expr_str = "foo >= bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::GreaterEqual,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_less() {
        let expr_str = "foo < bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::Less,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_greater() {
        let expr_str = "foo > bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::Greater,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    /// This should not compile in the end, but it will parse fine
    /// TODO: maybe we should think about parsing this should lead to error
    #[test]
    fn boolean_primary_compare_greater_equal_with_literal_semantically_invalid() {
        let expr_str = "foo >= 'bar'";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::GreaterEqual,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Constant(Constant::String("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_literal_to_literal() {
        let expr_str = "'foo' == 'bar'";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::Equal,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Constant(Constant::String("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Constant(Constant::String("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_not_equal() {
        let expr_str = "foo != bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::NotEqual,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_equal_not() {
        let expr_str = "foo == !bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::Equal,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::NotSecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    /// This looks weird, but grammar allows this (makes sense, when we think of boolean
    /// values in terms of 0 and 1).
    #[test]
    fn boolean_primary_compare_greater_equal_not() {
        let expr_str = "foo >= !bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::GreaterEqual,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::NotSecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn boolean_primary_compare_greater_equal_not_first() {
        let expr_str = "!foo >= bar";

        let expected =
            ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                comp_op: ComparisonOperator::GreaterEqual,
                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::NotSecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    }),
                )),
                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                    ComplementFactor::SecondFactor(SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    }),
                )),
            }));

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    // according to the x++-grammar this is NOT parsable
    // -> this, however, IS parsable: !(!foo) ==  !(!bar)
    #[test]
    fn boolean_primary_compare_not_not_both_sides_err() {
        let expr_str = "!!foo == !!bar;";

        // FIXME: make this right-recursive to avoid endless recursion!
        let output = conditional_expr_chumsky().parse(expr_str).into_result();

        assert!(output.is_err());
    }

    #[test]
    fn and_expr_simple() {
        let expr_str = "foo && bar";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            ))),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    },
                ))),
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn or_expr_simple() {
        let expr_str = "foo || bar";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            ))),
            log_op: LogicalOperator::Or,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    },
                ))),
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn and_expr_not() {
        let expr_str = "foo && !bar";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            ))),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::NotSecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("bar".to_owned())),
                    },
                ))),
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn and_or_not_expr() {
        let expr_str = "foo && bar || !baz";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::ConditionalExpression(
                ConditionalExpressionData {
                    cond_expr_left: Box::new(ConditionalExpression::Expression(
                        Expression::Simple(SimpleExpression::Term(Term::ComplementFactor(
                            ComplementFactor::SecondFactor(SecondFactor {
                                sign_op: None,
                                factor: Factor::Field(Field::Id("foo".to_owned())),
                            }),
                        ))),
                    )),
                    log_op: LogicalOperator::And,
                    cond_expr_right: Box::new(ConditionalExpression::Expression(
                        Expression::Simple(SimpleExpression::Term(Term::ComplementFactor(
                            ComplementFactor::SecondFactor(SecondFactor {
                                sign_op: None,
                                factor: Factor::Field(Field::Id("bar".to_owned())),
                            }),
                        ))),
                    )),
                },
            )),
            log_op: LogicalOperator::Or,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::NotSecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("baz".to_owned())),
                    },
                ))),
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn or_and_expr() {
        let expr_str = "foo || bar && baz";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::ConditionalExpression(
                ConditionalExpressionData {
                    cond_expr_left: Box::new(
                        Factor::Field(Field::Id("foo".to_owned())).builder().into(),
                    ),
                    log_op: LogicalOperator::Or,
                    cond_expr_right: Box::new(
                        Factor::Field(Field::Id("bar".to_owned())).builder().into(),
                    ),
                },
            )),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(Factor::Field(Field::Id("baz".to_owned())).builder().into()),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn or_or_and_expr() {
        let expr_str = "foo || bar || baz && quux";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::ConditionalExpression(
                ConditionalExpressionData {
                    cond_expr_left: Box::new(ConditionalExpression::ConditionalExpression(
                        ConditionalExpressionData {
                            cond_expr_left: Box::new(
                                Factor::Field(Field::Id("foo".to_owned())).builder().into(),
                            ),
                            log_op: LogicalOperator::Or,
                            cond_expr_right: Box::new(
                                Factor::Field(Field::Id("bar".to_owned())).builder().into(),
                            ),
                        },
                    )),
                    log_op: LogicalOperator::Or,
                    cond_expr_right: Box::new(
                        Factor::Field(Field::Id("baz".to_owned())).builder().into(),
                    ),
                },
            )),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(Factor::Field(Field::Id("quux".to_owned())).builder().into()),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn and_or_and_expr() {
        let expr_str = "foo && bar || baz && quux";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::ConditionalExpression(
                ConditionalExpressionData {
                    cond_expr_left: Box::new(ConditionalExpression::ConditionalExpression(
                        ConditionalExpressionData {
                            cond_expr_left: Box::new(
                                Factor::Field(Field::Id("foo".to_owned())).builder().into(),
                            ),
                            log_op: LogicalOperator::And,
                            cond_expr_right: Box::new(
                                Factor::Field(Field::Id("bar".to_owned())).builder().into(),
                            ),
                        },
                    )),
                    log_op: LogicalOperator::Or,
                    cond_expr_right: Box::new(
                        Factor::Field(Field::Id("baz".to_owned())).builder().into(),
                    ),
                },
            )),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(Factor::Field(Field::Id("quux".to_owned())).builder().into()),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn and_expr_parenthesized_or() {
        let expr_str = "foo && (bar || baz)";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            ))),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::ConditionalExpression(
                                ConditionalExpression::ConditionalExpression(
                                    ConditionalExpressionData {
                                        cond_expr_left: Box::new(
                                            ConditionalExpression::Expression(Expression::Simple(
                                                SimpleExpression::Term(Term::ComplementFactor(
                                                    ComplementFactor::SecondFactor(SecondFactor {
                                                        sign_op: None,
                                                        factor: Factor::Field(Field::Id(
                                                            "bar".to_owned(),
                                                        )),
                                                    }),
                                                )),
                                            )),
                                        ),
                                        log_op: LogicalOperator::Or,
                                        cond_expr_right: Box::new(
                                            ConditionalExpression::Expression(Expression::Simple(
                                                SimpleExpression::Term(Term::ComplementFactor(
                                                    ComplementFactor::SecondFactor(SecondFactor {
                                                        sign_op: None,
                                                        factor: Factor::Field(Field::Id(
                                                            "baz".to_owned(),
                                                        )),
                                                    }),
                                                )),
                                            )),
                                        ),
                                    },
                                ),
                            ),
                        )),
                    },
                ))),
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn logical_comparison_mixed_symmetrical_expr() {
        let expr_str = "foo <= bar && baz == quux";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Comparison(
                ComparisonExpression {
                    simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("foo".to_owned())),
                        }),
                    )),
                    comp_op: ComparisonOperator::LessEqual,
                    simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("bar".to_owned())),
                        }),
                    )),
                },
            ))),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Comparison(
                ComparisonExpression {
                    simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("baz".to_owned())),
                        }),
                    )),
                    comp_op: ComparisonOperator::Equal,
                    simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("quux".to_owned())),
                        }),
                    )),
                },
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn logical_comparison_mixed_asymmetrical_expr() {
        let expr_str = "foo <= bar && baz";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Comparison(
                ComparisonExpression {
                    simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("foo".to_owned())),
                        }),
                    )),
                    comp_op: ComparisonOperator::LessEqual,
                    simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("bar".to_owned())),
                        }),
                    )),
                },
            ))),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("baz".to_owned())),
                    },
                ))),
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn logical_comparison_mixed_different_order_asymmetrical_expr() {
        let expr_str = "foo && bar <= baz";

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(Expression::Simple(
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor: Factor::Field(Field::Id("foo".to_owned())),
                    },
                ))),
            ))),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(Expression::Comparison(
                ComparisonExpression {
                    simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("bar".to_owned())),
                        }),
                    )),
                    comp_op: ComparisonOperator::LessEqual,
                    simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                        ComplementFactor::SecondFactor(SecondFactor {
                            sign_op: None,
                            factor: Factor::Field(Field::Id("baz".to_owned())),
                        }),
                    )),
                },
            ))),
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }

    #[test]
    fn logical_comparison_mixed_deep_parenthesized_expr() {
        let expr_str = r#"foo
            && (bar < baz
                || !(quux.foo == baz && 'woot' == trix))"#;

        let expected = ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
            cond_expr_left: Box::new(ConditionalExpression::Expression(
                Expression::Simple(
                    SimpleExpression::Term(
                        Term::ComplementFactor(
                            ComplementFactor::SecondFactor(SecondFactor {
                                sign_op: None,
                                factor: Factor::Field(Field::Id("foo".to_owned()))
                            }))))
            )),
            log_op: LogicalOperator::And,
            cond_expr_right: Box::new(ConditionalExpression::Expression(
                Expression::Simple(
                    SimpleExpression::Term(
                        Term::ComplementFactor(
                            ComplementFactor::SecondFactor(SecondFactor {
                                sign_op: None,
                                factor: Factor::ParenthesizedTernary(
                                    Box::new(TernaryExpression::ConditionalExpression(
                                        ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
                                            cond_expr_left: Box::new(ConditionalExpression::Expression(
                                                Expression::Comparison(ComparisonExpression {
                                                    simple_expr_left: SimpleExpression::Term(
                                                        Term::ComplementFactor(
                                                            ComplementFactor::SecondFactor(SecondFactor {
                                                                sign_op: None,
                                                                factor: Factor::Field(Field::Id("bar".to_owned()))
                                                            })
                                                        )
                                                    ),
                                                    comp_op: ComparisonOperator::Less,
                                                    simple_expr_right: SimpleExpression::Term(
                                                        Term::ComplementFactor(
                                                            ComplementFactor::SecondFactor(SecondFactor {
                                                                sign_op: None,
                                                                factor: Factor::Field(Field::Id("baz".to_owned()))
                                                            })
                                                        )
                                                    )
                                                })
                                            )),
                                            log_op: LogicalOperator::Or,
                                            cond_expr_right: Box::new(ConditionalExpression::Expression(
                                                Expression::Simple(
                                                    SimpleExpression::Term(
                                                        Term::ComplementFactor(
                                                            ComplementFactor::NotSecondFactor(
                                                                SecondFactor {
                                                                    sign_op: None,
                                                                    factor: Factor::ParenthesizedTernary(Box::new(TernaryExpression::ConditionalExpression(
                                                                        ConditionalExpression::ConditionalExpression(ConditionalExpressionData {
                                                                            cond_expr_left: Box::new(ConditionalExpression::Expression(
                                                                                Expression::Comparison(ComparisonExpression {
                                                                                    simple_expr_left: SimpleExpression::Term(
                                                                                        Term::ComplementFactor(
                                                                                            ComplementFactor::SecondFactor(SecondFactor {
                                                                                                sign_op: None,
                                                                                                factor: Factor::Field(Field::QualifierId(
                                                                                                    Qualifier::Id("quux".to_owned()),
                                                                                                    "foo".to_owned()
                                                                                                ))
                                                                                            })
                                                                                        )
                                                                                    ),
                                                                                    comp_op: ComparisonOperator::Equal,
                                                                                    simple_expr_right: SimpleExpression::Term(
                                                                                        Term::ComplementFactor(
                                                                                            ComplementFactor::SecondFactor(SecondFactor {
                                                                                                sign_op: None,
                                                                                                factor: Factor::Field(Field::Id("baz".to_owned()))
                                                                                            })
                                                                                        )
                                                                                    )
                                                                                })
                                                                            )),
                                                                            log_op: LogicalOperator::And,
                                                                            cond_expr_right: Box::new(ConditionalExpression::Expression(
                                                                                Expression::Comparison(ComparisonExpression {
                                                                                    simple_expr_left: SimpleExpression::Term(
                                                                                        Term::ComplementFactor(
                                                                                            ComplementFactor::SecondFactor(SecondFactor {
                                                                                                sign_op: None,
                                                                                                factor: Factor::Constant(Constant::String("woot".to_owned()))
                                                                                            })
                                                                                        )
                                                                                    ),
                                                                                    comp_op: ComparisonOperator::Equal,
                                                                                    simple_expr_right: SimpleExpression::Term(
                                                                                        Term::ComplementFactor(
                                                                                            ComplementFactor::SecondFactor(SecondFactor {
                                                                                                sign_op: None,
                                                                                                factor: Factor::Field(Field::Id("trix".to_owned()))
                                                                                            })
                                                                                        )
                                                                                    )
                                                                                })
                                                                            )),
                                                                        })
                                                                    )))
                                                                }
                                                            )
                                                        )
                                                    )
                                                )
                                            ))
                                        })
                                    ))
                                )
                            })))
                )
            ))
        });

        assert_parse_chumsky(expr_str, &expected, &conditional_expr_chumsky);
    }
}
