use chumsky::error::Rich;
use chumsky::text::whitespace;
use chumsky::{extra, Parser};

use dracaena::ast::order_group::*;

use super::common::keyword_no_case;
use super::field_column::fields_in_order_group_chumsky;

fn group_by_chumsky<'a>(
) -> impl Parser<'a, &'a str, GroupOrderBy, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case("group")
        .ignore_then(whitespace().at_least(1))
        .ignore_then(
            keyword_no_case("by")
                .ignore_then(whitespace().at_least(1))
                .or_not(),
        )
        .then(fields_in_order_group_chumsky())
        .map(|(opt_by, order_elems)| GroupOrderBy {
            by: opt_by.is_some(),
            order_elems,
        })
}

fn order_by_chumsky<'a>(
) -> impl Parser<'a, &'a str, GroupOrderBy, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case("order")
        .ignore_then(whitespace().at_least(1))
        .ignore_then(
            keyword_no_case("by")
                .ignore_then(whitespace().at_least(1))
                .or_not(),
        )
        .then(fields_in_order_group_chumsky())
        .map(|(opt_by, order_elems)| GroupOrderBy {
            by: opt_by.is_some(),
            order_elems,
        })
}

pub fn order_group_chumsky<'a>(
) -> impl Parser<'a, &'a str, OrderGroupData, extra::Err<Rich<'a, char>>> + Clone {
    order_by_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(group_by_chumsky())
                .or_not(),
        )
        .map(|(order_by, group_by)| OrderGroupData {
            order_group: OrderGroup::OrderBy(order_by),
            other_order_group: group_by.map(OrderGroup::GroupBy),
        })
        .or(group_by_chumsky()
            .then(
                whitespace()
                    .at_least(1)
                    .ignore_then(order_by_chumsky())
                    .or_not(),
            )
            .map(|(group_by, order_by)| OrderGroupData {
                order_group: OrderGroup::GroupBy(group_by),
                other_order_group: order_by.map(OrderGroup::OrderBy),
            }))
}

#[cfg(test)]
mod tests {

    use dracaena::ast::order_elem::{Direction, OrderElem};

    use super::*;
    use crate::parsers::common::assert_parse_chumsky;

    #[test]
    fn group_by_order_by_separate_one_field() {
        let fields_str = "foo";
        let group_by_clause = format!("group by {}", fields_str);
        let order_by_clause = format!("order by {}", fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![OrderElem {
                table: None,
                field: "foo".to_owned(),
                direction: None,
                index: None,
            }],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: None,
        };

        let order_by_parsed = OrderGroupData {
            order_group: OrderGroup::OrderBy(group_order_by),
            other_order_group: None,
        };

        assert_parse_chumsky(&group_by_clause, &group_by_parsed, &order_group_chumsky);
        assert_parse_chumsky(&order_by_clause, &order_by_parsed, &order_group_chumsky);
    }

    #[test]
    fn group_by_order_by_together_one_field() {
        let fields_str = "foo";
        let group_by_order_by_clause = format!("group by {} order by {}", fields_str, fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![OrderElem {
                table: None,
                field: "foo".to_owned(),
                direction: None,
                index: None,
            }],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: Some(OrderGroup::OrderBy(group_order_by)),
        };

        assert_parse_chumsky(
            &group_by_order_by_clause,
            &group_by_parsed,
            &order_group_chumsky,
        );
    }

    #[test]
    fn group_by_order_by_separate_multiple_fields() {
        let fields_str = "foo, myTable.bar";
        let group_by_clause = format!("group by {}", fields_str);
        let order_by_clause = format!("order by {}", fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: None,
                    index: None,
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: None,
                    index: None,
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: None,
        };

        let order_by_parsed = OrderGroupData {
            order_group: OrderGroup::OrderBy(group_order_by),
            other_order_group: None,
        };

        assert_parse_chumsky(&group_by_clause, &group_by_parsed, &order_group_chumsky);
        assert_parse_chumsky(&order_by_clause, &order_by_parsed, &order_group_chumsky);
    }

    #[test]
    fn group_by_order_by_together_multiple_fields() {
        let fields_str = "foo, myTable.bar";
        let group_by_order_by_clause = format!("group by {} order by {}", fields_str, fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: None,
                    index: None,
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: None,
                    index: None,
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: Some(OrderGroup::OrderBy(group_order_by)),
        };

        assert_parse_chumsky(
            &group_by_order_by_clause,
            &group_by_parsed,
            &order_group_chumsky,
        );
    }

    #[test]
    fn group_by_order_by_separate_multiple_fields_with_direction() {
        let fields_str = "foo asc, myTable.bar desc";
        let group_by_clause = format!("group by {}", fields_str);
        let order_by_clause = format!("order by {}", fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: Some(Direction::Ascending),
                    index: None,
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: Some(Direction::Descending),
                    index: None,
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: None,
        };

        let order_by_parsed = OrderGroupData {
            order_group: OrderGroup::OrderBy(group_order_by),
            other_order_group: None,
        };

        assert_parse_chumsky(&group_by_clause, &group_by_parsed, &order_group_chumsky);
        assert_parse_chumsky(&order_by_clause, &order_by_parsed, &order_group_chumsky);
    }

    #[test]
    fn group_by_order_by_together_multiple_fields_with_direction() {
        let fields_str = "foo desc, myTable.bar asc";
        let group_by_order_by_clause = format!("group by {} order by {}", fields_str, fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: Some(Direction::Descending),
                    index: None,
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: Some(Direction::Ascending),
                    index: None,
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: Some(OrderGroup::OrderBy(group_order_by)),
        };

        assert_parse_chumsky(
            &group_by_order_by_clause,
            &group_by_parsed,
            &order_group_chumsky,
        );
    }

    #[test]
    fn group_by_order_by_separate_multiple_fields_with_index_and_direction() {
        let fields_str = "foo[0] asc, myTable.bar[42] desc";
        let group_by_clause = format!("group by {}", fields_str);
        let order_by_clause = format!("order by {}", fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: Some(Direction::Ascending),
                    index: Some(0),
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: Some(Direction::Descending),
                    index: Some(42),
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: None,
        };

        let order_by_parsed = OrderGroupData {
            order_group: OrderGroup::OrderBy(group_order_by),
            other_order_group: None,
        };

        assert_parse_chumsky(&group_by_clause, &group_by_parsed, &order_group_chumsky);
        assert_parse_chumsky(&order_by_clause, &order_by_parsed, &order_group_chumsky);
    }

    #[test]
    fn group_by_order_by_together_multiple_fields_with_index_and_direction() {
        let fields_str = "foo[0] desc, myTable.bar[42] asc";
        let group_by_order_by_clause = format!("group by {} order by {}", fields_str, fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: Some(Direction::Descending),
                    index: Some(0),
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: Some(Direction::Ascending),
                    index: Some(42),
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(group_order_by.clone()),
            other_order_group: Some(OrderGroup::OrderBy(group_order_by)),
        };

        assert_parse_chumsky(
            &group_by_order_by_clause,
            &group_by_parsed,
            &order_group_chumsky,
        );
    }

    #[test]
    /// We just change the order of `group by` and `order by`, so that we can see
    /// whether it basically works. No need here to repeat all the tests
    fn order_by_group_by_together_multiple_fields_with_index_and_direction() {
        let fields_str = "foo[0] desc, myTable.bar[42] asc";
        let group_by_order_by_clause = format!("order by {} group by {}", fields_str, fields_str);

        let group_order_by = GroupOrderBy {
            by: true,
            order_elems: vec![
                OrderElem {
                    table: None,
                    field: "foo".to_owned(),
                    direction: Some(Direction::Descending),
                    index: Some(0),
                },
                OrderElem {
                    table: Some("myTable".to_owned()),
                    field: "bar".to_owned(),
                    direction: Some(Direction::Ascending),
                    index: Some(42),
                },
            ],
        };

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::OrderBy(group_order_by.clone()),
            other_order_group: Some(OrderGroup::GroupBy(group_order_by)),
        };

        assert_parse_chumsky(
            &group_by_order_by_clause,
            &group_by_parsed,
            &order_group_chumsky,
        );
    }

    #[test]
    fn group_by_order_by_together_different_fields() {
        let fields_str_group_by = "foo, myTable.bar";
        let fields_str_order_by = "quux, corge";
        let group_by_order_by_clause = format!(
            "group by {} order by {}",
            fields_str_group_by, fields_str_order_by
        );

        let group_by_parsed = OrderGroupData {
            order_group: OrderGroup::GroupBy(GroupOrderBy {
                by: true,
                order_elems: vec![
                    OrderElem {
                        table: None,
                        field: "foo".to_owned(),
                        direction: None,
                        index: None,
                    },
                    OrderElem {
                        table: Some("myTable".to_owned()),
                        field: "bar".to_owned(),
                        direction: None,
                        index: None,
                    },
                ],
            }),
            other_order_group: Some(OrderGroup::OrderBy(GroupOrderBy {
                by: true,
                order_elems: vec![
                    OrderElem {
                        table: None,
                        field: "quux".to_owned(),
                        direction: None,
                        index: None,
                    },
                    OrderElem {
                        table: None,
                        field: "corge".to_owned(),
                        direction: None,
                        index: None,
                    },
                ],
            })),
        };

        assert_parse_chumsky(
            &group_by_order_by_clause,
            &group_by_parsed,
            &order_group_chumsky,
        );
    }

    #[test]
    fn same_order_group_twice_should_err() {
        let fields_str = "foo desc, myTable.bar asc";
        let group_by_twice = format!("group by {} group by {}", fields_str, fields_str);

        assert!(order_group_chumsky()
            .parse(group_by_twice.as_str())
            .has_errors());
    }
}
