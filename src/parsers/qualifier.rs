use chumsky::prelude::*;
use dracaena::ast::qualifier::Qualifier;

use super::common::xpp_identifier_chumsky;
use super::eval::eval_chumsky;

pub fn qualifier_chumsky<'a>(
) -> impl Parser<'a, &'a str, Qualifier, extra::Err<Rich<'a, char>>> + Clone {
    xpp_identifier_chumsky()
        .then_ignore(just('.'))
        .map(|id| Qualifier::Id(id.to_string()))
        .or(eval_chumsky()
            .then_ignore(just('.'))
            .map(|eval| Qualifier::Eval(Box::new(eval))))
}
