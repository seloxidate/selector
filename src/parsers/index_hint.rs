use chumsky::{error::Rich, extra, text::whitespace, Parser};
use dracaena::ast::index_hint::IndexHint;

use super::common::{keyword_no_case, xpp_identifier_chumsky};

pub fn index_hint_chumsky<'a>(
) -> impl Parser<'a, &'a str, IndexHint, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case("index")
        .ignore_then(whitespace().at_least(1))
        .ignore_then(
            keyword_no_case("hint")
                .ignore_then(whitespace().at_least(1))
                .or_not(),
        )
        .then(xpp_identifier_chumsky())
        .map(|(hint, id)| IndexHint {
            hint: hint.is_some(),
            id: id.to_string(),
        })
}
