use chumsky::prelude::*;
use chumsky::primitive::recurse;
use chumsky::text::whitespace;
use chumsky::Parser;

use dracaena::ast::find::{
    FindJoin, FindOrder, FindTable, FindUsing, FindWhere, SelectStmt, SelectTable,
};

use super::common::keyword_no_case;
use super::index_hint::index_hint_chumsky;
use super::join::join_list_chumsky;
use super::order_group::order_group_chumsky;
use super::select_option::select_option_chumsky;
use super::table::table_chumsky;
use super::ternary_expression::ternary_expr_chumsky;

fn select_table_chumsky<'a>(
) -> impl Parser<'a, &'a str, SelectTable, extra::Err<Rich<'a, char>>> + Clone {
    keyword_no_case("while")
        .ignore_then(whitespace().at_least(1))
        .ignore_then(select_chumsky())
        .map(SelectTable::WhileSelect)
        .or(find_table_chumsky().map(SelectTable::SimpleSelect))
}

fn select_chumsky<'a>() -> impl Parser<'a, &'a str, SelectStmt, extra::Err<Rich<'a, char>>> + Clone
{
    keyword_no_case("select")
        .ignore_then(whitespace().at_least(1))
        .ignore_then(
            select_option_chumsky()
                .then_ignore(whitespace().at_least(1))
                .or_not(),
        )
        .then(table_chumsky())
        .map(|(select_opts, table)| {
            SelectStmt {
                // TODO: revisit this: is this still true with the `chumsky` rewrite?
                // FIXME: it is weird: select_option always has Some(...) even, when we have no select option parsed;
                // that's why we have to use this hack here
                select_opts: select_opts.filter(|so| !so.is_empty()),
                table,
            }
        })
}

fn find_table_chumsky<'a>(
) -> impl Parser<'a, &'a str, FindTable, extra::Err<Rich<'a, char>>> + Clone {
    select_chumsky()
        .map(FindTable::Select)
        .or(keyword_no_case("delete")
            .ignore_then(whitespace().at_least(1))
            .ignore_then(
                select_option_chumsky()
                    .then_ignore(whitespace().at_least(1))
                    .or_not(),
            )
            .then(table_chumsky())
            .map(|(select_opts, table)| FindTable::Delete(SelectStmt { select_opts, table })))
}

fn find_using_chumsky<'a>(
) -> impl Parser<'a, &'a str, FindUsing, extra::Err<Rich<'a, char>>> + Clone {
    select_table_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(index_hint_chumsky())
                .or_not(),
        )
        .map(|(select_table, index_hint)| FindUsing {
            select_table,
            index_hint,
        })
}

fn find_order_chumsky<'a>(
) -> impl Parser<'a, &'a str, FindOrder, extra::Err<Rich<'a, char>>> + Clone {
    find_using_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(order_group_chumsky())
                .or_not(),
        )
        .map(|(find_using, order_group)| FindOrder {
            find_using,
            order_group,
        })
}

fn find_where_chumsky<'a>(
) -> impl Parser<'a, &'a str, FindWhere, extra::Err<Rich<'a, char>>> + Clone {
    find_order_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(keyword_no_case("where"))
                .ignore_then(whitespace().at_least(1))
                .ignore_then(recurse(ternary_expr_chumsky))
                .or_not(),
        )
        .map(|(find_order, if_expr)| FindWhere {
            find_order,
            if_expr,
        })
}

pub fn find_join_chumsky<'a>(
) -> impl Parser<'a, &'a str, FindJoin, extra::Err<Rich<'a, char>>> + Clone {
    find_where_chumsky()
        .then(
            whitespace()
                .at_least(1)
                .ignore_then(join_list_chumsky())
                .or_not(),
        )
        .map(|(find_where, join_list)| FindJoin {
            find_where,
            join_list: join_list.filter(|jl| !jl.is_empty()),
        })
}

pub fn find_stmt_chumsky<'a>(
) -> impl Parser<'a, &'a str, FindJoin, extra::Err<Rich<'a, char>>> + Clone {
    find_join_chumsky()
        .then(whitespace().ignore_then(just(';')).or_not())
        // we can have whitespace at the end of our whole select statement
        .then_ignore(whitespace())
        .try_map(|(find_join, semicol), span| {
            if find_join.is_while_select() && semicol.is_some() {
                Err(Rich::custom(
                    span,
                    "No semicolon allowed after 'while select'",
                ))
            } else {
                Ok(find_join)
            }
        })
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::parsers::common::assert_parse_chumsky;

    use dracaena::ast::constant::Constant;
    use dracaena::ast::expression::{
        ComparisonExpression, ConditionalExpression, Expression, SimpleExpression,
        TernaryExpression,
    };
    use dracaena::ast::factor::*;
    use dracaena::ast::field::Field;
    use dracaena::ast::join::{JoinClause, JoinOrder, JoinSpec, JoinUsing};
    use dracaena::ast::operator::*;
    use dracaena::ast::order_elem::{Direction, OrderElem};
    use dracaena::ast::order_group::GroupOrderBy;
    use dracaena::ast::order_group::OrderGroup;
    use dracaena::ast::order_group::OrderGroupData;
    use dracaena::ast::qualifier::Qualifier;
    use dracaena::ast::select_option::FirstOnly;
    use dracaena::ast::select_option::SelectOpts;
    use dracaena::ast::table::Table;
    use dracaena::ast::term::Term;

    #[test]
    fn find_join_simple_select() {
        let find_join_stmt = "select custTable";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_simple_select_with_select_op() {
        let find_join_stmt = "select firstonly custTable";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: Some(vec![SelectOpts::FirstOnly(FirstOnly::FirstOnly)]),
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_where() {
        let find_join_stmt = "select custTable where custTable.Foo == 'bar'";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Foo".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Constant(Constant::String("bar".to_owned())),
                                }),
                            )),
                        },
                    )),
                )),
            },
            join_list: None,
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_order_group() {
        let find_join_stmt = r#"select custTable
            group by custTable.Foo"#;
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: Some(OrderGroupData {
                        order_group: OrderGroup::GroupBy(GroupOrderBy {
                            by: true,
                            order_elems: vec![OrderElem {
                                table: Some("custTable".to_owned()),
                                field: "Foo".to_owned(),
                                direction: None,
                                index: None,
                            }],
                        }),
                        other_order_group: None,
                    }),
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_order_group_and_where() {
        let find_join_stmt = r#"select custTable
            group by custTable.Foo
            where custTable.Bar == 'baz'"#;
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: Some(OrderGroupData {
                        order_group: OrderGroup::GroupBy(GroupOrderBy {
                            by: true,
                            order_elems: vec![OrderElem {
                                table: Some("custTable".to_owned()),
                                field: "Foo".to_owned(),
                                direction: None,
                                index: None,
                            }],
                        }),
                        other_order_group: None,
                    }),
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Bar".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Constant(Constant::String("baz".to_owned())),
                                }),
                            )),
                        },
                    )),
                )),
            },
            join_list: None,
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_join_list() {
        let find_join_stmt = r#"select custTable
            join dirParty
                where dirParty.RecId == custTable.Party
            join logisticAddress
                where logisticAddress.RecId == dirParty.Address"#;
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: Some(vec![
                JoinSpec {
                    join_order: JoinOrder {
                        join_using: JoinUsing {
                            join_clause: JoinClause {
                                join_variant: None,
                                select_opts: None,
                                table: Table::Standalone("dirParty".to_owned()),
                            },
                            index_hint: None,
                        },
                        order_group: None,
                    },
                    if_expr: Some(TernaryExpression::ConditionalExpression(
                        ConditionalExpression::Expression(Expression::Comparison(
                            ComparisonExpression {
                                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("dirParty".to_owned()),
                                            "RecId".to_owned(),
                                        )),
                                    }),
                                )),
                                comp_op: ComparisonOperator::Equal,
                                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("custTable".to_owned()),
                                            "Party".to_owned(),
                                        )),
                                    }),
                                )),
                            },
                        )),
                    )),
                },
                JoinSpec {
                    join_order: JoinOrder {
                        join_using: JoinUsing {
                            join_clause: JoinClause {
                                join_variant: None,
                                select_opts: None,
                                table: Table::Standalone("logisticAddress".to_owned()),
                            },
                            index_hint: None,
                        },
                        order_group: None,
                    },
                    if_expr: Some(TernaryExpression::ConditionalExpression(
                        ConditionalExpression::Expression(Expression::Comparison(
                            ComparisonExpression {
                                simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("logisticAddress".to_owned()),
                                            "RecId".to_owned(),
                                        )),
                                    }),
                                )),
                                comp_op: ComparisonOperator::Equal,
                                simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Field(Field::QualifierId(
                                            Qualifier::Id("dirParty".to_owned()),
                                            "Address".to_owned(),
                                        )),
                                    }),
                                )),
                            },
                        )),
                    )),
                },
            ]),
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_where_and_join_list() {
        let find_join_stmt = r#"select custTable
            where custTable.Foo == 'bar'
            join dirParty
                where dirParty.RecId == custTable.Party"#;
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Foo".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Constant(Constant::String("bar".to_owned())),
                                }),
                            )),
                        },
                    )),
                )),
            },
            join_list: Some(vec![JoinSpec {
                join_order: JoinOrder {
                    join_using: JoinUsing {
                        join_clause: JoinClause {
                            join_variant: None,
                            select_opts: None,
                            table: Table::Standalone("dirParty".to_owned()),
                        },
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("dirParty".to_owned()),
                                        "RecId".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Party".to_owned(),
                                    )),
                                }),
                            )),
                        },
                    )),
                )),
            }]),
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_order_group_and_where_and_join_list() {
        let find_join_stmt = r#"select custTable
            group by custTable.Bar desc
            order by custTable.Name asc
            where custTable.Foo == 'bar'
            join dirParty
                where dirParty.RecId == custTable.Party"#;
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: Some(OrderGroupData {
                        order_group: OrderGroup::GroupBy(GroupOrderBy {
                            by: true,
                            order_elems: vec![OrderElem {
                                table: Some("custTable".to_owned()),
                                field: "Bar".to_owned(),
                                direction: Some(Direction::Descending),
                                index: None,
                            }],
                        }),
                        other_order_group: Some(OrderGroup::OrderBy(GroupOrderBy {
                            by: true,
                            order_elems: vec![OrderElem {
                                table: Some("custTable".to_owned()),
                                field: "Name".to_owned(),
                                direction: Some(Direction::Ascending),
                                index: None,
                            }],
                        })),
                    }),
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Foo".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Constant(Constant::String("bar".to_owned())),
                                }),
                            )),
                        },
                    )),
                )),
            },
            join_list: Some(vec![JoinSpec {
                join_order: JoinOrder {
                    join_using: JoinUsing {
                        join_clause: JoinClause {
                            join_variant: None,
                            select_opts: None,
                            table: Table::Standalone("dirParty".to_owned()),
                        },
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("dirParty".to_owned()),
                                        "RecId".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Party".to_owned(),
                                    )),
                                }),
                            )),
                        },
                    )),
                )),
            }]),
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_where_and_join_list_multiple_joins_with_parenthesized_expression() {
        let find_join_stmt = r#"select custTable
            where custTable.Foo == 'bar'
            join dirParty
                where (dirParty.RecId == custTable.Party)
            join logisticsLocation
                where (logisticsLocation.RecId == dirParty.Location)"#;
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: Some(TernaryExpression::ConditionalExpression(
                    ConditionalExpression::Expression(Expression::Comparison(
                        ComparisonExpression {
                            simple_expr_left: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Field(Field::QualifierId(
                                        Qualifier::Id("custTable".to_owned()),
                                        "Foo".to_owned(),
                                    )),
                                }),
                            )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right: SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: Factor::Constant(Constant::String("bar".to_owned())),
                                }),
                            )),
                        },
                    )),
                )),
            },
            join_list: Some(vec![
                JoinSpec {
                    join_order: JoinOrder {
                        join_using: JoinUsing {
                            join_clause: JoinClause {
                                join_variant: None,
                                select_opts: None,
                                table: Table::Standalone("dirParty".to_owned()),
                            },
                            index_hint: None,
                        },
                        order_group: None,
                    },
                    if_expr: Some(TernaryExpression::ConditionalExpression(
                        Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::ConditionalExpression(
                                ConditionalExpression::Expression(Expression::Comparison(
                                    ComparisonExpression {
                                        simple_expr_left: SimpleExpression::Term(
                                            Term::ComplementFactor(ComplementFactor::SecondFactor(
                                                SecondFactor {
                                                    sign_op: None,
                                                    factor: Factor::Field(Field::QualifierId(
                                                        Qualifier::Id("dirParty".to_owned()),
                                                        "RecId".to_owned(),
                                                    )),
                                                },
                                            )),
                                        ),
                                        comp_op: ComparisonOperator::Equal,
                                        simple_expr_right: SimpleExpression::Term(
                                            Term::ComplementFactor(ComplementFactor::SecondFactor(
                                                SecondFactor {
                                                    sign_op: None,
                                                    factor: Factor::Field(Field::QualifierId(
                                                        Qualifier::Id("custTable".to_owned()),
                                                        "Party".to_owned(),
                                                    )),
                                                },
                                            )),
                                        ),
                                    },
                                )),
                            ),
                        ))
                        .builder()
                        .into(),
                    )),
                },
                JoinSpec {
                    join_order: JoinOrder {
                        join_using: JoinUsing {
                            join_clause: JoinClause {
                                join_variant: None,
                                select_opts: None,
                                table: Table::Standalone("logisticsLocation".to_owned()),
                            },
                            index_hint: None,
                        },
                        order_group: None,
                    },
                    if_expr: Some(TernaryExpression::ConditionalExpression(
                        Factor::ParenthesizedTernary(Box::new(
                            TernaryExpression::ConditionalExpression(
                                ConditionalExpression::Expression(Expression::Comparison(
                                    ComparisonExpression {
                                        simple_expr_left: SimpleExpression::Term(
                                            Term::ComplementFactor(ComplementFactor::SecondFactor(
                                                SecondFactor {
                                                    sign_op: None,
                                                    factor: Factor::Field(Field::QualifierId(
                                                        Qualifier::Id(
                                                            "logisticsLocation".to_owned(),
                                                        ),
                                                        "RecId".to_owned(),
                                                    )),
                                                },
                                            )),
                                        ),
                                        comp_op: ComparisonOperator::Equal,
                                        simple_expr_right: SimpleExpression::Term(
                                            Term::ComplementFactor(ComplementFactor::SecondFactor(
                                                SecondFactor {
                                                    sign_op: None,
                                                    factor: Factor::Field(Field::QualifierId(
                                                        Qualifier::Id("dirParty".to_owned()),
                                                        "Location".to_owned(),
                                                    )),
                                                },
                                            )),
                                        ),
                                    },
                                )),
                            ),
                        ))
                        .builder()
                        .into(),
                    )),
                },
            ]),
        };
        assert_parse_chumsky(find_join_stmt, &ast_expected, &find_join_chumsky);
    }

    #[test]
    fn find_join_with_where_before_order_group_and_join_list_err() {
        let partial_select_not_consumed = r#" order by custTable.Name asc
            join dirParty
                where dirParty.RecId == custTable.Party"#;
        // should not be parsed correctly, as `where` comes before `order group`
        let find_join_stmt = format!(
            r#"select custTable where custTable.Foo == 'bar'{}"#,
            partial_select_not_consumed
        );

        let result = find_join_chumsky()
            .parse(find_join_stmt.as_str())
            .into_result();
        assert!(result.is_err());
    }

    /// We only test a simple example, because we have already tested `find_join` extensively
    #[test]
    fn find_stmt_simple_select() {
        let find_stmt_str = "select custTable;";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_stmt_str, &ast_expected, &find_stmt_chumsky);
    }

    /// We only test a simple example, because we have already tested `find_join` extensively
    #[test]
    fn find_stmt_simple_select_with_whitespace_before_semicolon() {
        let find_stmt_str = "select custTable     ;";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_stmt_str, &ast_expected, &find_stmt_chumsky);
    }

    /// We only test a simple example, because we have already tested `find_join` extensively
    #[test]
    fn find_stmt_simple_select_with_whitespace_before_and_after_semicolon() {
        let find_stmt_str = "select custTable ; ";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::SimpleSelect(FindTable::Select(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        })),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_stmt_str, &ast_expected, &find_stmt_chumsky);
    }

    /// We only test a simple example, because we have already tested `find_join` extensively
    #[test]
    fn find_stmt_while_select() {
        let find_stmt_str = "while select custTable";
        let ast_expected = FindJoin {
            find_where: FindWhere {
                find_order: FindOrder {
                    find_using: FindUsing {
                        select_table: SelectTable::WhileSelect(SelectStmt {
                            select_opts: None,
                            table: Table::Standalone("custTable".to_owned()),
                        }),
                        index_hint: None,
                    },
                    order_group: None,
                },
                if_expr: None,
            },
            join_list: None,
        };
        assert_parse_chumsky(find_stmt_str, &ast_expected, &find_stmt_chumsky);
    }
}
