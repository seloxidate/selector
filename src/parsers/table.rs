use chumsky::error::Rich;
use chumsky::text::whitespace;
use chumsky::{extra, Parser};

use dracaena::ast::table::*;

use std::str;

use super::common::xpp_identifier_chumsky;
use super::field_column::field_list_chumsky;
use super::from::from_chumsky;

pub fn table_chumsky<'a>() -> impl Parser<'a, &'a str, Table, extra::Err<Rich<'a, char>>> + Clone {
    field_list_chumsky()
        .then_ignore(whitespace().at_least(1))
        .then(from_chumsky())
        .map(|(fields, from)| Table::WithFields(TableWithFields { fields, from }))
        .or(xpp_identifier_chumsky().map(|ident| Table::Standalone(ident.to_string())))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::from::From;
    use dracaena::ast::{
        column::{Column, FunctionExpression},
        field_column::FieldList,
    };

    #[test]
    fn table_standalone() {
        let table_str = "custTable";

        let expected = Table::Standalone("custTable".to_owned());

        assert_parse_chumsky(table_str, &expected, &table_chumsky);
    }

    #[test]
    fn table_with_fields() {
        let table_str = "RecId, Name from CustTable";

        let expected = Table::WithFields(TableWithFields {
            fields: vec![
                FieldList::Col(Column {
                    name: "RecId".to_owned(),
                    function: None,
                    index: None,
                }),
                FieldList::Col(Column {
                    name: "Name".to_owned(),
                    function: None,
                    index: None,
                }),
            ],
            from: From {
                table: "CustTable".to_owned(),
            },
        });

        assert_parse_chumsky(table_str, &expected, &table_chumsky);
    }

    #[test]
    fn table_with_star() {
        let table_str = "* from CustTable";

        let expected = Table::WithFields(TableWithFields {
            fields: vec![FieldList::All],
            from: From {
                table: "CustTable".to_owned(),
            },
        });

        assert_parse_chumsky(table_str, &expected, &table_chumsky);
    }

    #[test]
    fn table_with_aggregate() {
        let table_str = "sum(amount) from CustTable";

        let expected = Table::WithFields(TableWithFields {
            fields: vec![FieldList::Col(Column::from(FunctionExpression::Sum(
                "amount".to_owned(),
            )))],
            from: From {
                table: "CustTable".to_owned(),
            },
        });

        assert_parse_chumsky(table_str, &expected, &table_chumsky);
    }
}
