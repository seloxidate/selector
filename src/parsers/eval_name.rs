use chumsky::prelude::*;
use chumsky::primitive::just;
use chumsky::recursive::recursive;
use chumsky::text::whitespace;
use dracaena::ast::eval::Eval;
use dracaena::ast::eval_name::EvalName;
use dracaena::ast::parm::ParmElem;
use dracaena::ast::qualifier::Qualifier;

use std::rc::Rc;

use super::common::{keyword_no_case, xpp_identifier_chumsky};
use super::parm::parm_list_chumsky;

pub fn eval_name_chumsky<'a>(
) -> impl Parser<'a, &'a str, (EvalName, Option<Option<Vec<ParmElem>>>), extra::Err<Rich<'a, char>>>
       + Clone {
    choice((
        keyword_no_case("super")
            .ignore_then(just('('))
            .ignore_then(eval_name_right_recursive_chumsky().or_not())
            .map(|eval_name_rr| convert_to_eval_name(EvalName::Super, eval_name_rr)),
        xpp_identifier_chumsky()
            .then_ignore(just('('))
            .then(eval_name_right_recursive_chumsky().or_not())
            .map(|(id, eval_name_rr)| {
                convert_to_eval_name(EvalName::Id(id.to_string()), eval_name_rr)
            }),
        xpp_identifier_chumsky()
            .then_ignore(just("::"))
            .then(xpp_identifier_chumsky())
            .then_ignore(just('('))
            .then(eval_name_right_recursive_chumsky().or_not())
            .map(|((id1, id2), eval_name_rr)| {
                convert_to_eval_name(
                    EvalName::IdDblColonId(id1.to_string(), id2.to_string()),
                    eval_name_rr,
                )
            }),
        xpp_identifier_chumsky()
            .then_ignore(just("."))
            .then(xpp_identifier_chumsky())
            .then_ignore(just('('))
            .then(eval_name_right_recursive_chumsky().or_not())
            .map(|((id1, id2), eval_name_rr)| {
                convert_to_eval_name(
                    EvalName::Qualifier(Qualifier::Id(id1.to_string()), id2.to_string()),
                    eval_name_rr,
                )
            }),
        xpp_identifier_chumsky()
            .then_ignore(just('.'))
            .then(xpp_identifier_chumsky())
            .then_ignore(just("::"))
            .then(xpp_identifier_chumsky())
            .then_ignore(just('('))
            .then(eval_name_right_recursive_chumsky().or_not())
            .map(|(((id1, id2), id3), eval_name_rr)| {
                convert_to_eval_name(
                    EvalName::QualifierDblColon(
                        Qualifier::Id(id1.to_string()),
                        id2.to_string(),
                        id3.to_string(),
                    ),
                    eval_name_rr,
                )
            }),
        keyword_no_case("new")
            .ignore_then(whitespace().at_least(1))
            .ignore_then(xpp_identifier_chumsky())
            .then_ignore(just('('))
            .then(eval_name_right_recursive_chumsky().or_not())
            .map(|(id, eval_name_rr)| {
                convert_to_eval_name(EvalName::New(id.to_string()), eval_name_rr)
            }),
    ))
    // for better compile times
    .boxed()
}

fn convert_to_eval_name(
    eval_name: EvalName,
    eval_name_right: Option<EvalNameRightRecursive>,
) -> (EvalName, Option<Option<Vec<ParmElem>>>) {
    if let Some(enr) = eval_name_right {
        enr.into_eval_name(eval_name)
    } else {
        (eval_name, None)
    }
}

fn eval_name_right_recursive_chumsky<'a>(
) -> impl Parser<'a, &'a str, EvalNameRightRecursive, extra::Err<Rich<'a, char>>> + Clone {
    recursive(|tree| {
        parm_list_chumsky()
            .map(Rc::new)
            .then(
                just('.')
                    .ignore_then(xpp_identifier_chumsky())
                    .then_ignore(just('('))
                    // TODO: we probably don't want to `clone` here; maybe use `Rc`?
                    .then(tree.clone().or_not())
                    .map(|(method_id, eval_name_rr)| {
                        let tmp: Option<EvalNameRightRecursive> = eval_name_rr;
                        EvalNameRightRecursive::PeriodMethodCall {
                            parm_list: Rc::default(),
                            method_id: String::from(method_id),
                            eval_name_right_recursive: tmp.map(Box::new),
                        }
                    })
                    .or(just('.')
                        .ignore_then(xpp_identifier_chumsky())
                        .then_ignore(just("::"))
                        .then(xpp_identifier_chumsky())
                        .then_ignore(just('('))
                        .then(tree.or_not())
                        .map(|((quali_id, method_id), eval_name_rr)| {
                            EvalNameRightRecursive::DblColonMethodCall {
                                parm_list: Rc::default(),
                                qualifier_id: String::from(quali_id),
                                method_id: String::from(method_id),
                                eval_name_right_recursive: eval_name_rr.map(Box::new),
                            }
                        })),
            )
            .map(
                |(parm_list, mut eval_name_rr): (
                    Rc<Option<Vec<ParmElem>>>,
                    EvalNameRightRecursive,
                )| {
                    std::mem::replace(
                        eval_name_rr.set_parm_list(parm_list),
                        EvalNameRightRecursive::ParmListOnly {
                            parm_list: Rc::default(),
                        },
                    )
                },
            )
    })
}

enum EvalNameRightRecursive {
    ParmListOnly {
        parm_list: Rc<Option<Vec<ParmElem>>>,
    },
    PeriodMethodCall {
        parm_list: Rc<Option<Vec<ParmElem>>>,
        method_id: String,
        eval_name_right_recursive: Option<Box<EvalNameRightRecursive>>,
    },
    DblColonMethodCall {
        parm_list: Rc<Option<Vec<ParmElem>>>,
        qualifier_id: String,
        method_id: String,
        eval_name_right_recursive: Option<Box<EvalNameRightRecursive>>,
    },
}

impl EvalNameRightRecursive {
    fn set_parm_list(&mut self, parm_list: Rc<Option<Vec<ParmElem>>>) -> &mut Self {
        match self {
            EvalNameRightRecursive::ParmListOnly { parm_list: pl } => *pl = parm_list,
            EvalNameRightRecursive::PeriodMethodCall {
                parm_list: pl,
                method_id: _,
                eval_name_right_recursive: _,
            } => *pl = parm_list,
            EvalNameRightRecursive::DblColonMethodCall {
                parm_list: pl,
                qualifier_id: _,
                method_id: _,
                eval_name_right_recursive: _,
            } => *pl = parm_list,
        }
        self
    }

    pub fn into_eval_name(self, eval_name: EvalName) -> (EvalName, Option<Option<Vec<ParmElem>>>) {
        match self {
            EvalNameRightRecursive::ParmListOnly { parm_list } => {
                (
                    eval_name,
                    Some(
                        match Rc::try_unwrap(parm_list).map_err(|pl| (*pl).clone()) {
                            // normally, this should have single ownership;
                            Ok(pl) => pl,
                            // only, if we have done something wrong with our ownership model,
                            // parm_list is a shared reference, so we clone the underlying
                            // Rc's value (see map_err above)
                            Err(pl) => pl,
                        },
                    ),
                )
            }
            EvalNameRightRecursive::PeriodMethodCall {
                parm_list,
                method_id,
                eval_name_right_recursive,
            } => {
                let first = EvalName::Qualifier(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name,
                        parm_list: match Rc::try_unwrap(parm_list).map_err(|pl| (*pl).clone()) {
                            // normally, this should have single ownership;
                            Ok(pl) => pl,
                            // only, if we have done something wrong with our ownership model,
                            // parm_list is a shared reference, so we clone the underlying
                            // Rc's value (see map_err above)
                            Err(pl) => pl,
                        },
                    })),
                    method_id,
                );

                if let Some(eval_name_rr) = eval_name_right_recursive {
                    eval_name_rr.into_eval_name(first)
                } else {
                    (first, None)
                }
            }
            EvalNameRightRecursive::DblColonMethodCall {
                parm_list,
                qualifier_id,
                method_id,
                eval_name_right_recursive,
            } => {
                let first = EvalName::QualifierDblColon(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name,
                        parm_list: match Rc::try_unwrap(parm_list).map_err(|pl| (*pl).clone()) {
                            // normally, this should have single ownership;
                            Ok(pl) => pl,
                            // only, if we have done something wrong with our ownership model,
                            // parm_list is a shared reference, so we clone the underlying
                            // Rc's value (see map_err above)
                            Err(pl) => pl,
                        },
                    })),
                    qualifier_id,
                    method_id,
                );

                if let Some(eval_name_rr) = eval_name_right_recursive {
                    eval_name_rr.into_eval_name(first)
                } else {
                    (first, None)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::qualifier::Qualifier;

    #[test]
    fn eval_name_std_id() {
        assert_parse_chumsky(
            "myId(",
            &(EvalName::Id("myId".to_owned()), None),
            &eval_name_chumsky,
        );
    }

    #[test]
    fn eval_name_std_id_dblcolon_std_id() {
        assert_parse_chumsky(
            "myClass::myStaticFunction(",
            &(
                EvalName::IdDblColonId("myClass".to_owned(), "myStaticFunction".to_owned()),
                None,
            ),
            &eval_name_chumsky,
        );
    }

    #[test]
    fn eval_name_super() {
        assert_parse_chumsky("super(", &(EvalName::Super, None), &eval_name_chumsky);
    }

    #[test]
    fn eval_name_super_case_insensitive() {
        assert_parse_chumsky("suPeR(", &(EvalName::Super, None), &eval_name_chumsky);
    }

    #[test]
    fn eval_name_new() {
        assert_parse_chumsky(
            "new MyClass(",
            &(EvalName::New("MyClass".to_owned()), None),
            &eval_name_chumsky,
        );
    }

    #[test]
    fn eval_name_looks_like_new_but_is_normal_identifier() {
        assert_parse_chumsky(
            "newMyClass(",
            &(EvalName::Id("newMyClass".to_owned()), None),
            &eval_name_chumsky,
        );
    }

    #[test]
    fn eval_name_qualifier() {
        assert_parse_chumsky(
            "myClass.myMethod(",
            &(
                EvalName::Qualifier(Qualifier::Id("myClass".to_owned()), "myMethod".to_owned()),
                None,
            ),
            &eval_name_chumsky,
        );
    }

    #[test]
    fn eval_name_qualifier_dblcolon() {
        assert_parse_chumsky(
            "myTableMap.myTable::myMethod(",
            &(
                EvalName::QualifierDblColon(
                    Qualifier::Id("myTableMap".to_owned()),
                    "myTable".to_owned(),
                    "myMethod".to_owned(),
                ),
                None,
            ),
            &eval_name_chumsky,
        );
    }
}
