use chumsky::{
    error::Error,
    extra::ParserExtra,
    input::{StrInput, ValueInput},
    prelude::*,
    text::{ascii::ident, Char},
};
use dracaena::ast::constant::Constant;
#[cfg(test)]
use pretty_assertions::assert_eq;
use std::{
    fmt::Debug,
    str::{self},
};

pub fn assert_parse_chumsky<
    'a,
    T: Debug + PartialEq,
    E: ParserExtra<'a, &'a str>,
    P: Parser<'a, &'a str, T, E>,
>(
    stmt_to_parse: &'a str,
    ast_expected: &T,
    parse_fn: &dyn Fn() -> P,
) where
    <E as ParserExtra<'a, &'a str>>::State: Default,
    <E as ParserExtra<'a, &'a str>>::Context: Default,
    <E as ParserExtra<'a, &'a str>>::Error: PartialEq + Debug,
{
    let parsed = parse_fn().parse(stmt_to_parse);

    let parsed_result = parsed.into_result();

    assert_eq!(Ok(ast_expected), parsed_result.as_ref());
}

pub fn column_idx_chumsky<'a>() -> impl Parser<'a, &'a str, u32, extra::Err<Rich<'a, char>>> + Clone
{
    let unsigned_number = text::int::<_, _, _>(10);
    unsigned_number
        .padded()
        // we want to have padding at the first `[`, but _not_ at the right side of `]`
        .delimited_by(just("[").padded(), just("]"))
        .from_str::<u32>()
        .validate(|idx, e, emitter| match idx {
            Ok(idx) => idx,
            Err(parse_int_err) => {
                emitter.emit(Rich::custom(
                    e.span(),
                    format!(
                        "Parsing of column index failed. Reason: {:#?}",
                        parse_int_err.kind()
                    ),
                ));
                // we have no meaningful value here to return
                Default::default()
            }
        })
}

pub fn xpp_identifier_chumsky<'a>(
) -> impl Parser<'a, &'a str, &'a <char as Char>::Str, extra::Err<Rich<'a, char>>> + Clone {
    text::ident()
}

// TODO: we must also consider escaping special chars!

pub fn literal_chumsky<'a>(
) -> impl Parser<'a, &'a str, Constant, extra::Err<Rich<'a, char>>> + Clone {
    fn _literal<'a>(
        enclosing_char: char,
    ) -> impl Parser<'a, &'a str, String, extra::Err<Rich<'a, char>>> + Clone {
        let escape = just(['\\', enclosing_char]).to(enclosing_char);
        none_of::<_, _, extra::Err<Rich<char>>>(enclosing_char)
            .and_is(escape.not())
            .or(escape)
            .repeated()
            .collect::<String>()
            .padded_by(just(enclosing_char))
    }
    _literal('"').or(_literal('\'')).map(Constant::String)
}

/// A case-insensitive variant of chumsky::text::keyword
pub fn keyword_no_case<
    'a,
    I: ValueInput<'a> + StrInput<'a, C>,
    C: Char<Str = str> + 'a,
    Str: AsRef<C::Str> + 'a + Clone,
    E: ParserExtra<'a, I> + 'a,
>(
    keyword: Str,
) -> impl Parser<'a, I, &'a C::Str, E> + Clone + 'a
where
    C::Str: PartialEq,
{
    ident()
        .try_map(move |s: &C::Str, span| {
            s.eq_ignore_ascii_case(keyword.as_ref())
                .then_some(())
                .ok_or_else(|| Error::expected_found(None, None, span))
        })
        .to_slice()
}

#[cfg(test)]
mod tests {
    use chumsky::{error::Rich, Parser};
    use dracaena::ast::constant::Constant;
    use pretty_assertions::assert_eq;

    use crate::parsers::common::{column_idx_chumsky, literal_chumsky, xpp_identifier_chumsky};

    #[test]
    fn column_idx_zero_works() {
        let to_parse = format!("[{}]", 0u32);
        assert_eq!(
            column_idx_chumsky().parse(to_parse.as_str()).into_result(),
            Ok(0)
        );
    }

    #[test]
    fn column_idx_whitespace_max_u32_works() {
        let to_parse = format!(" [ {} ]", u32::MAX);
        assert_eq!(
            column_idx_chumsky().parse(to_parse.as_str()).into_result(),
            Ok(u32::MAX)
        );
    }

    #[test]
    fn column_idx_overflow_error() {
        let to_parse = format!("[ {} ]", u64::from(u32::MAX) + 1);
        assert_eq!(
            column_idx_chumsky().parse(to_parse.as_str()).into_result(),
            Err(vec![Rich::<char>::custom(
                (0..14).into(),
                "Parsing of column index failed. Reason: PosOverflow"
            )])
        );
    }

    #[test]
    fn xpp_identifier() {
        assert_eq!(
            xpp_identifier_chumsky()
                .parse("my_ident2_parse")
                .into_result(),
            Ok("my_ident2_parse")
        );
    }

    #[test]
    fn xpp_identifier_with_additional_content_at_end() {
        assert!(xpp_identifier_chumsky().parse("foo bar").has_errors());
    }

    #[test]
    fn literal_without_escape() {
        assert_eq!(
            literal_chumsky()
                .parse(r#""a raw string literal""#)
                .into_result(),
            Ok(Constant::String("a raw string literal".into()))
        );

        assert_eq!(
            literal_chumsky()
                .parse(r#"'a raw string literal'"#)
                .into_result(),
            Ok(Constant::String("a raw string literal".into()))
        );
    }

    #[test]
    fn literal_with_escape_quotes() {
        assert_eq!(
            literal_chumsky()
                .parse(r#""a raw \"string\" literal""#)
                .into_result(),
            Ok(Constant::String(r#"a raw "string" literal"#.into()))
        );

        assert_eq!(
            literal_chumsky()
                .parse(r#"'a raw \'string\' literal'"#)
                .into_result(),
            Ok(Constant::String(r#"a raw 'string' literal"#.into()))
        );
    }

    #[test]
    fn literal_without_escaping_whitespace() {
        assert_eq!(
            literal_chumsky()
                .parse(r#""a raw \nstring\t literal""#)
                .into_result(),
            Ok(Constant::String(r#"a raw \nstring\t literal"#.into()))
        );
    }

    #[test]
    fn literal_with_escaping_whitespace() {
        assert_eq!(
            literal_chumsky()
                .parse(r#""a raw \\nstring\\t literal""#)
                .into_result(),
            Ok(Constant::String(r#"a raw \\nstring\\t literal"#.into()))
        );
    }
}
