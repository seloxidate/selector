use chumsky::error::Rich;
use chumsky::{
    extra,
    primitive::{choice, just},
    text, IterParser, Parser,
};
use dracaena::ast::constant::Constant;

use std::{num::ParseIntError, str};

use super::common::{keyword_no_case, literal_chumsky, xpp_identifier_chumsky};

enum IntErrorCustom {
    ParseIntError(ParseIntError),
    OverflowIntError,
}

pub fn constant_chumsky<'a>(
) -> impl Parser<'a, &'a str, Constant, extra::Err<Rich<'a, char>>> + Clone {
    let int = text::int(10)
        .from_str::<i32>()
        .map(|res| res.map_err(IntErrorCustom::ParseIntError))
        .then_ignore(just(".").not());
    choice((
        just('+')
            .to(1)
            .or(just('-').to(-1))
            .repeated()
            .foldr(int, |a: i32, b| {
                b.and_then(|b| a.checked_mul(b).ok_or(IntErrorCustom::OverflowIntError))
            })
            .validate(|int, e, emitter| match int {
                Ok(idx) => idx,
                Err(IntErrorCustom::ParseIntError(parse_int_err)) => {
                    emitter.emit(Rich::custom(
                        e.span(),
                        format!("Parsing of i32 failed. Reason: {:#?}", parse_int_err.kind()),
                    ));
                    // we have no meaningful value here to return
                    Default::default()
                }
                Err(IntErrorCustom::OverflowIntError) => {
                    emitter.emit(Rich::custom(
                        e.span(),
                        "Parsing of i32 failed. Reason: Negative overflow".to_string(),
                    ));
                    // we have no meaningful value here to return
                    Default::default()
                }
            })
            .map(Constant::Integer),
        text::int(10)
            .then(just('.').then(text::digits(10)).or_not())
            .to_slice()
            .from_str()
            .unwrapped()
            .map(Constant::Double),
        // TODO: parse int64 as well
        literal_chumsky(),
        xpp_identifier_chumsky()
            .then_ignore(just("::").padded())
            .then(xpp_identifier_chumsky())
            .map(|(name, value)| Constant::Enum(name.to_string(), value.to_string())),
        keyword_no_case("true").to(Constant::True),
        keyword_no_case("false").to(Constant::False),
        keyword_no_case("null").to(Constant::Null),
        // TODO: parse TableMap (once we can parse `qualifier`, this will be possible)
    ))
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::qualifier::Qualifier;

    #[test]
    fn constant_parse_single_digit() {
        assert_parse_chumsky("4", &Constant::Integer(4), &constant_chumsky);
    }

    #[test]
    fn constant_parse_double_digit() {
        assert_parse_chumsky("42", &Constant::Integer(42), &constant_chumsky);
    }

    #[test]
    fn constant_parse_single_digit_whitespace_err() {
        assert!(constant_chumsky().parse("4 ").has_errors());
    }

    #[test]
    fn constant_parse_leading0_digit_err() {
        assert!(constant_chumsky().parse("042").has_errors());
    }

    #[test]
    fn constant_parse_0_digit() {
        assert_parse_chumsky("0", &Constant::Integer(0), &constant_chumsky);
    }

    #[test]
    fn constant_parse_double() {
        assert_parse_chumsky("0.5", &Constant::Double(0.5), &constant_chumsky);
    }

    #[ignore = "We are not able to parse this yet with chumsky"]
    #[test]
    fn constant_parse_double_with_exponent() {
        assert_parse_chumsky("0.5e5", &Constant::Double(50000.0), &constant_chumsky);
    }

    #[ignore = "We are not able to parse this yet with chumsky"]
    #[test]
    fn constant_parse_double_with_exponent_negative() {
        assert_parse_chumsky("0.5e-5", &Constant::Double(0.000005), &constant_chumsky);
    }

    #[ignore = "We are not able to parse this yet with chumsky"]
    #[test]
    fn constant_parse_double_with_exponent_prefix_0() {
        assert_parse_chumsky("0.5e05", &Constant::Double(50000.0), &constant_chumsky);
    }

    #[test]
    fn constant_parse_literal_string_double_quotes() {
        assert_parse_chumsky(
            r#""42""#,
            &Constant::String("42".to_owned()),
            &constant_chumsky,
        );
    }

    #[test]
    fn constant_parse_literal_string_single_quotes() {
        assert_parse_chumsky(
            r#"'42'"#,
            &Constant::String("42".to_owned()),
            &constant_chumsky,
        );
    }

    #[test]
    fn constant_parse_literal_string_empty() {
        assert_parse_chumsky(r#"''"#, &Constant::String("".to_owned()), &constant_chumsky);
    }

    #[test]
    fn constant_parse_true_false_null() {
        assert_parse_chumsky("true", &Constant::True, &constant_chumsky);
        assert_parse_chumsky("TrUe", &Constant::True, &constant_chumsky);

        assert_parse_chumsky("false", &Constant::False, &constant_chumsky);
        assert_parse_chumsky("faLSe", &Constant::False, &constant_chumsky);

        assert_parse_chumsky("null", &Constant::Null, &constant_chumsky);
        assert_parse_chumsky("NuLl", &Constant::Null, &constant_chumsky);
    }

    #[test]
    fn constant_parse_enum() {
        assert_parse_chumsky(
            "MyEnum::MyValue",
            &Constant::Enum("MyEnum".to_owned(), "MyValue".to_owned()),
            &constant_chumsky,
        );
    }

    #[ignore = "We are not able to parse this yet with chumsky"]
    #[test]
    fn constant_parse_table_map() {
        assert_parse_chumsky(
            "TableMap.Table::Field",
            &Constant::TableMap(
                Qualifier::Id("TableMap".to_owned()),
                "Table".to_owned(),
                "Field".to_owned(),
            ),
            &constant_chumsky,
        );
    }
}
