use chumsky::prelude::*;
use dracaena::ast::eval::Eval;

use super::eval_name::eval_name_chumsky;
use super::parm::parm_list_chumsky;

pub fn eval_chumsky<'a>() -> impl Parser<'a, &'a str, Eval, extra::Err<Rich<'a, char>>> + Clone {
    eval_name_chumsky()
        .then(parm_list_chumsky())
        .try_map(|(eval_name, parm_list), span| {
            if eval_name.1.is_some() && parm_list.is_some() {
                Err(Rich::custom(
                    span,
                    "Failure when parsing eval_name and parm_list".to_string(),
                ))
            } else {
                Ok(Eval {
                    eval_name: eval_name.0,
                    parm_list: parm_list.or(eval_name.1.flatten()),
                })
            }
        })
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::parsers::common::assert_parse_chumsky;
    use dracaena::ast::constant::Constant;
    use dracaena::ast::eval_name::EvalName;

    use dracaena::ast::factor::Factor;

    use dracaena::ast::parm::ParmElem;
    use dracaena::ast::qualifier::Qualifier;

    #[test]
    fn eval_function_args_non() {
        assert_parse_chumsky(
            "myFn()",
            &Eval {
                eval_name: EvalName::Id("myFn".to_owned()),
                parm_list: None,
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_function_args_simple() {
        assert_parse_chumsky(
            r#"myFn("some String")"#,
            &Eval {
                eval_name: EvalName::Id("myFn".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("some String".to_owned()))
                        .builder()
                        .into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_static_method_args_simple() {
        assert_parse_chumsky(
            r#"Class::construct("some String")"#,
            &Eval {
                eval_name: EvalName::IdDblColonId("Class".to_owned(), "construct".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("some String".to_owned()))
                        .builder()
                        .into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_new_method_args_simple() {
        assert_parse_chumsky(
            "new Class(false)",
            &Eval {
                eval_name: EvalName::New("Class".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::False).builder().into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_qualifier_method_args_simple() {
        assert_parse_chumsky(
            "myClass.myMethod(false)",
            &Eval {
                eval_name: EvalName::Qualifier(
                    Qualifier::Id("myClass".to_owned()),
                    "myMethod".to_owned(),
                ),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::False).builder().into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_qualifier_method_chained() {
        assert_parse_chumsky(
            "foo().bar()",
            &Eval {
                eval_name: EvalName::Qualifier(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name: EvalName::Id("foo".to_owned()),
                        parm_list: None,
                    })),
                    "bar".to_owned(),
                ),
                parm_list: None,
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_qualifier_with_table_map_method() {
        assert_parse_chumsky(
            "foo().bar::baz(Class::construct())",
            &Eval {
                eval_name: EvalName::QualifierDblColon(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name: EvalName::Id("foo".to_owned()),
                        parm_list: None,
                    })),
                    "bar".to_owned(),
                    "baz".to_owned(),
                ),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Eval(Eval {
                        eval_name: EvalName::IdDblColonId(
                            "Class".to_owned(),
                            "construct".to_owned(),
                        ),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_qualifier_method_chained_with_args() {
        assert_parse_chumsky(
            "myClass.first(false).second(null).third(true)",
            &Eval {
                eval_name: EvalName::Qualifier(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Eval(Box::new(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("myClass".to_owned()),
                                    "first".to_owned(),
                                ),
                                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                    Factor::Constant(Constant::False).builder().into(),
                                ))]),
                            })),
                            "second".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Null).builder().into(),
                        ))]),
                    })),
                    "third".to_owned(),
                ),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::True).builder().into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_function_args_nested() {
        assert_parse_chumsky(
            "myFn(Class::construct())",
            &Eval {
                eval_name: EvalName::Id("myFn".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Eval(Eval {
                        eval_name: EvalName::IdDblColonId(
                            "Class".to_owned(),
                            "construct".to_owned(),
                        ),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                ))]),
            },
            &eval_chumsky,
        );
    }

    #[test]
    fn eval_function_args_deeply_nested() {
        assert_parse_chumsky(
            "myFn(Class::construct(super(true)))",
            &Eval {
                eval_name: EvalName::Id("myFn".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Eval(Eval {
                        eval_name: EvalName::IdDblColonId(
                            "Class".to_owned(),
                            "construct".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Super,
                                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                    Factor::Constant(Constant::True).builder().into(),
                                ))]),
                            })
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                ))]),
            },
            &eval_chumsky,
        );
    }
}
